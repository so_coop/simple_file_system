#include "bitmap.h"
#include "disk_driver.h"
#include "simplefs.h"


#define N1 25
#define N2 35
#define N3 94
#define N4 13
#define N5 1
#define N6 0

#define N7 255 //11111111
#define N8 0 //00000000
#define N9 173 //10101101
#define N10 146 //10010010
#define N11 73 //1001001
#define N12 192 //11000000

#define TEXT0 "There is no place like 127.0.0.1. (Anonymous)"
#define TEXT1 "The best thing about a boolean is even if you are wrong, you are only off by a bit. (Anonymous)"
#define TEXT2 "It’s not a bug – it’s an undocumented feature. (Anonymous)"
#define TEXT3 "Programming is like sex. One mistake and you have to support it for the rest of your life. (Michael Sinz)"
#define TEXT4 "If debugging is the process of removing software bugs, then programming must be the process of putting them in. (Edsger Dijkstra)"
#define TEXT5 "Talk is cheap. Show me the code. (Linus Torvalds)"
#define TEXT6 "#include <stdio.h> #define max 10 int a[11] = { 10, 14, 19, 26, 27, 31, 33, 35, 42, 44, 0 }; int b[10];  void merging(int low, int mid, int high) { int l1, l2, i; for(l1 = low, l2 = mid + 1, i = low; l1 <= mid && l2 <= high; i++) { if(a[l1] <= a[l2]) b[i] = a[l1++]; else b[i] = a[l2++]; }   while(l1 <= mid)    b[i++] = a[l1++]; while(l2 <= high)   b[i++] = a[l2++]; for(i = low; i <= high; i++) a[i] = b[i]; } void sort(int low, int high) { int mid; if(low < high) { mid = (low + high) / 2; sort(low, mid); sort(mid+1, high); merging(low, mid, high); } else { return; }   } int main() { int i; printf(\"List before sorting\n\"); for(i = 0; i <= max; i++) printf(\"%d \", a[i]); sort(0, max); printf(\"\nList after sorting\n\"); for(i = 0; i <= max; i++) printf(\"%d \", a[i]); }"
#define TEXT7 "FEAR OF THE DARK - IRON MAIDEN I am a man who walks alone And when I'm walking a dark road At night or strolling through the park When the light begins to change I sometimes feel a little strange A little anxious when it's dark Fear of the dark, fear of the dark I have a constant fear that something's always near Fear of the dark, fear of the dark I have a phobia that someone's always there Have you run your fingers down the wall And have you felt your neck skin crawl When you're searching for the light? Sometimes when you're scared to take a look At the corner of the room You've sensed that something's watching you Fear of the dark, fear of the dark I have a constant fear that something's always near Fear of the dark, fear of the dark I have a phobia that someone's always there Have you ever been alone at night Thought you heard footsteps behind And turned around and no one's there? And as you quicken up your pace You find it hard to look again Because you're sure there's someone there Fear of the dark, fear of the dark I have a constant fear that something's always near Fear of the dark, fear of the dark I have a phobia that someone's always there Fear of the dark, fear of the dark Fear of the dark, fear of the dark Fear of the dark, fear of the dark Fear of the dark, fear of the dark Watching horror films the night before Debating witches and folklores The unknown troubles on your mind Maybe your mind is playing tricks You sense, and suddenly eyes fix On dancing shadows from behind Fear of the dark, fear of the dark I have a constant fear that something's always near Fear of the dark, fear of the dark I have a phobia that someone's always there Fear of the dark, fear of the dark I have a constant fear that something's always near Fear of the dark, fear of the dark I have a phobia that someone's always there When I'm walking a dark road I am a man who walks alone"

// Bitmap_test
//BitMap* bitmap_test_new(BitMap* bitmap,int num, int num_bits);
int bitmap_test_get(BitMap* bitmap,int start, int status);
void bitmap_test_set(BitMap* bitmap, int pos, int status);


// DiskDriver_test
void DiskDriver_print_status_test(DiskDriver* disk);
void DiskDriver_print_buffers_test(char in_buffer[], char out_buffer[]);
int DiskDriver_writeBlock_test(DiskDriver* disk, void* src, int block_num);
int DiskDriver_readBlock_test(DiskDriver* disk, void* dest, int block_num);
void DiskDriver_freeBlock_test(DiskDriver* disk, int block_num);
int DiskDriver_getFreeBlock_test(DiskDriver* disk, int start);
void DiskDriver_flush_test(DiskDriver* disk);

// SimpleFS_test
#define DCBS_NUM 1000
#define NO 0
#define YES 1

FileHandle* SimpleFS_createFile_test(DirectoryHandle* d, const char* filename);
int SimpleFS_mkDir_test(DirectoryHandle* d, char* dirname);
int SimpleFS_readDir_test(DirectoryHandle* d);
void print_names(char** names);
FileHandle* SimpleFS_openFile_test(DirectoryHandle* d, const char* filename);
int SimpleFS_close_test(FileHandle* f, const char* filename);
int SimpleFS_read_test(FileHandle* f, void* data, int size);
int SimpleFS_write_test(FileHandle* f, void* data, int size);
int SimpleFS_seek_test(FileHandle* f, int pos);
int SimpleFS_changeDir_test(DirectoryHandle* d, char* dirname, ArrayEntry a[]);
int SimpleFS_remove_test(DirectoryHandle* d, char* dirname, ArrayEntry a[]);
int SimpleFS_pwd_test(DirectoryHandle* d, DirectoryHandle* d_clone, ArrayEntry a[]);
int SimpleFS_tree_test(DirectoryHandle* d, ArrayEntry a[]);
int SimpleFS_rename_test(DirectoryHandle* d, ArrayEntry a[], char* src, char* dest);

int add(ArrayEntry a[], Inode* e);
int arrayEntry_is_valid(ArrayEntry ae);
int free_dcbs(DiskDriver* disk, ArrayEntry a[]);
