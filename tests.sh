#!/bin/sh
#A: Default commands to change shell color
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# I: A simple script to execute our tests, one for terminal window, with a command

echo "${green}Compilazione dei file test.c .${reset}" #A: Preparation of file
make clean
make all
 
#A: if there are some problem check chmod 
echo "${green}Inizio bitmap test.${reset}"
./bitmap_test

echo "${green}Inizio disk driver test.${reset}"
./disk_driver_test 

echo "${green}Inizio simple file system test.${reset}"
./simplefs_test



