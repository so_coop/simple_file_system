// I: A simple shell to try our file system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bitmap.h"
#include "disk_driver.h"
#include "simplefs.h"
#include "tools.h"
#include "test.h"

#define MAX_INPUT_SIZE MAX_DATA_IN_DATA_BLOCK*10
#define MAX_OPTIONS_SIZE 4
#define CMD_SIZE 13
const char* CMD[CMD_SIZE] = {"cat", "cd", "disk","echo", "help", "ls", "mkdir", "mv", "pwd", "q", "rm", "touch", "tree"};

void cd(DirectoryHandle* d, char* dirname, ArrayEntry a[]) {
    if(dirname) {
        // I: Go to dirname
        SimpleFS_changeDir_test(d, dirname, a);
    }

    else {
        // I: By default go to root
        SimpleFS_changeDir_test(d, "/", a);
    }
}

// I: Display information about builtin commands sorted alphabetically
void help(char* s) {
    if(s) {
       if(!strcmp(s, "cd"))
        printf("cd: cd [dir]\nChange the shell working directory.\nChange the current directory to DIR.\nThe default DIR is the value of the HOME shell variable.\n");

       else if(!strcmp(s, "help"))
        printf("help: help [pattern ...]\nDisplay information about builtin commands.\nDisplays brief summaries of builtin commands.\nIf PATTERN is specified, gives detailed help on all commands matching PATTERN, otherwise the list of help topics is printed.\n");

       else if(!strcmp(s, "ls"))
        printf("ls: ls\nList information about the FILEs (the current directory by default). Sort entries alphabetically\n");

       else if(!strcmp(s, "mkdir"))
        printf("mkdir: mkdir DIRECTORY...\nCreate the DIRECTORY(ies), if they do not already exist.\n");

       else if(!strcmp(s, "mv"))
        printf("mv: mv SOURCE DEST\nRename SOURCE to DEST.\n");

       else if(!strcmp(s, "pwd"))
        printf("pwd: pwd\nPrint the name of the current working directory.\n");

       else if(!strcmp(s, "rm")) //Più file
        printf("rm: rm FILE...\nrm removes each specified file.  By default, it does not remove directories.\n");

       else if(!strcmp(s, "touch"))
        printf("touch: touch FILE...\nCreate the FILE(s), if they do not already exist.\n");

       else if(!strcmp(s, "tree"))
        printf("tree: tree\nTree is a recursive directory listing program that produces a depth indented listing of files.\n");

       else if(!strcmp(s, "q"))
        printf("q: q\nQuit the shell.\n");

       else if(!strcmp(s,"echo"))
       	printf("echo: echo [SHORT-OPTION]... [STRING]...\nEcho the STRING(s) to standard output. With '>' write text in file\n") ;

       else if(!strcmp(s,"cat"))
       	printf("cat: cat [FILE]...\nConcatenate FILE(s), or standard input, to standard output.\n");

       else if(!strcmp(s,"disk"))
       	printf("disk: disk\nPrints on standard output the disk status.\n");

       else
        printf("Invalid command (type help for more info)\n");
    }

    else {
    printf("These shell commands are defined internally. Type `help' to see this list.\nType `help name' to find out more about the function `name'.\n\n");
    // Sort by alphabetic names
    for(int i=0; i<CMD_SIZE; i++)
        printf("%s\n", CMD[i]);
    }
}

void ls(DirectoryHandle* d) {
    SimpleFS_readDir_test(d);
}

void mkdir(DirectoryHandle* d, char** s) {
    // I: s[0] = mkdir so I've to start from pos 1 of "s"
    // mkdir can create multiple dir with one cmd, max MAX_OPTIONS_SIZE-1
    for(int i=1; i<MAX_OPTIONS_SIZE; i++) {
        if(s[i])
            SimpleFS_mkDir_test(d, s[i]);
    }
}

void mv(DirectoryHandle* d, ArrayEntry a[], char** s) {
  // I: s[0] = mv so I've to start from pos 1 of "s"
  char* filename     = s[1];
  char* new_filename = s[2];
  SimpleFS_rename_test(d, a, filename, new_filename);
}

void pwd(DirectoryHandle* d,DirectoryHandle* d_clone, ArrayEntry a[]) {
  SimpleFS_pwd_test(d, d_clone, a);
}

void rm(DirectoryHandle *d, char** s) {
	//A: Check all file to remove max MAX_OPTIONS_SIZE-1
	ArrayEntry dcbs_to_be_freed[DCBS_NUM*5] = {{0}};
    for(int i=1; i<MAX_OPTIONS_SIZE; i++) {
        if(s[i])
            SimpleFS_remove_test(d, s[i],dcbs_to_be_freed) ;
    }
}

void touch(DirectoryHandle* d, char** s) {
    // I: s[0] = touch so I've to start from pos 1 of "s"
    // touch can create multiple dir with one cmd, max MAX_OPTIONS_SIZE-1
    //FileHandle* files[MAX_OPTIONS_SIZE];
    for(int i=1; i<MAX_OPTIONS_SIZE; i++) {
        if(s[i]){
            //files[i-1] =
            FileHandle* file = SimpleFS_createFile_test(d, s[i]);
            SimpleFS_close_test(file, s[i]);
            //A: There is in create an error message if filename is too long
            }
    }
}

void tree(DirectoryHandle* d, ArrayEntry a[]) {
  SimpleFS_tree_test(d, a);
}

void echo(DirectoryHandle* d, char** options ){
	if(options[2]!= NULL && !strcmp(options[2],">")){
			FileHandle* file = SimpleFS_openFile_test(d, options[3]);

			if(file != NULL){
                Inode* fcb = file->fcb;
                if(!fcb)    exit(1);

				SimpleFS_write_test(file, options[1], strlen(options[1]));
            	SimpleFS_close_test(file, fcb->fcb.name);
            } //A: Error messages are in the functions

            else
                printf("File doesn't exist\n");
	}
	else if((options[3]) != NULL){
		printf("Invalid options. See help 'echo' for more details.\n");
	}
	else if(options[1] != NULL){ //A: input is a single string
	 printf("%s\n",options[1]);
	}
	else
		printf("Invalid options. See help 'echo' for more details.\n");

}

void cat(DirectoryHandle* d, char* option){
    FileHandle* file = SimpleFS_openFile_test(d, option);
    Inode* fcb = file->fcb;
    if(!fcb)    exit(1);

    //int file_size = fcb->fcb.size_in_blocks;
    //printf("File_block: %d\n", fcb->fcb.size_in_blocks); // A: debug
    int file_size = fcb->fcb.size_in_bytes;
    //printf("file size: %d\n", file_size); // I: debug

    //char* out_buffer = calloc((file_size*MAX_DATA_IN_DATA_BLOCK)+1, sizeof(char));
    char* out_buffer = calloc(file_size+1, sizeof(char));

    if (SimpleFS_read_test(file, out_buffer, file_size/*MAX_DATA_IN_DATA_BLOCK*/) < 0) {
        free(out_buffer);
        SimpleFS_close_test(file, fcb->fcb.name);
        return;
    }

    printf("%s\n", out_buffer);

    free(out_buffer);
    SimpleFS_close_test(file, fcb->fcb.name);
}

void disk_(DiskDriver* disk) {
  DiskDriver_print_status_test(disk);
}

int main(int argc, char *argv[]) {
  printf("===================================\n");
  printf("SHELL TO USE OUR FS v1.0\n");
  printf("===================================\n");

  // I: It handles the loop
  int ctrl = 1;

  // I: Initing Disk & Fs
  SimpleFS fs;
  DiskDriver disk;
  const char* filename = "SimpleFS_disk.dat";
  int num_blocks = 1024;

  printf("Initing the disk...\n");
  // I: DiskDriver_init() throws & handles errors itself, it is a void func
  DiskDriver_init(&disk, filename, num_blocks);
  DiskDriver_flush_test(&disk);
  printf("File %s created/opened\n", filename);

  // I: Init an array of ArrayEntry where to store dcbs when a dir is changed.
  // The array content must be freed at the end of the test, otherwise we can acceed to freed blocks.
  // To add an element, use the add func who set properly the field "to_be_closed"
  ArrayEntry dcbs_to_be_freed[DCBS_NUM] = {{0}};

  printf("Initing the fs...\n");
  DirectoryHandle* current_dir = SimpleFS_init(&fs, &disk,dcbs_to_be_freed);
  if(!current_dir) {
    printf("Error: fs init failed\n");
    return -1;
  }

  DiskDriver_flush_test(&disk);
  printf("Fs inited with success\n");

  // I: This clone must be used in pwd & tree cmd, because they do side effect on the DirectoryHandle passed
  DirectoryHandle* dh_clone = malloc(sizeof(DirectoryHandle));


  do {
    char in[MAX_INPUT_SIZE];
    char* options[MAX_OPTIONS_SIZE] = {0};
    // I: Debug
    //for(int a=0; a<MAX_OPTIONS_SIZE; a++)
    //    printf("options[%d] = %s\n", a, options[a]);
    printf("\nType a command: ");
    fgets(in, MAX_INPUT_SIZE, stdin);

    // I: Removes \n at the end of the string
    in[strlen(in) - 1] = '\0';
    // I: Breaks input on space
    //const char d[1] = " ";

    // I: char *strtok(char *str, const char *delim)
    // str − The contents of this string are modified and broken into smaller strings (tokens).
    // delim − This is the C string containing the delimiters. These may vary from one call to another.
    // This function returns a pointer to the first token found in the string.
    // A null pointer is returned if there are no tokens left to retrieve
    // I: Get the first token
    char* token = strtok(in, " ");

    // I: Walk through other tokens
    int i = 0;
    while(token != NULL && i<MAX_OPTIONS_SIZE) {
      //printf("token = %s\n", token);
      options[i++] = token;
      //printf("options[%d] = %s\n", i-1, options[i-1]);
      token = strtok(NULL, " ");
    }
    //printf("i = %d\n", i);

    char* cmd = options[0];

    if(!strcmp(cmd, "cd") && i<=2) // I: At most two parameters
        cd(current_dir, options[1], dcbs_to_be_freed);

    else if(!strcmp(cmd, "disk") && i==1)
        disk_(&disk);

    else if(!strcmp(cmd, "help") && i<=2)
        help(options[1]);

    else if(!strcmp(cmd, "ls") && i==1) // I: Exactly one parameter
        ls(current_dir);

    else if(!strcmp(cmd, "mkdir") && i>=2) // I: At least two parameters
        mkdir(current_dir, options);

    else if(!strcmp(cmd, "mv") && i==3) // I: Exactly three parameters (rename)
        mv(current_dir, dcbs_to_be_freed, options);

    else if(!strcmp(cmd, "pwd") && i==1) { // I: Exactly one parameter
        *dh_clone = *current_dir;
        pwd(current_dir,dh_clone, dcbs_to_be_freed);
    }

    else if(!strcmp(cmd, "rm") && i>=2)  // A: Need more parameter for options
        rm(current_dir, options);

    else if(!strcmp(cmd, "touch") && i>=2) // A: Need more parameter
        touch(current_dir, options);

    else if(!strcmp(cmd, "tree") && i==1)  //A: Need 1 parameter
        tree(current_dir, dcbs_to_be_freed);


    else if(!strcmp(cmd, "echo") && (i==2 || i==4)) //A: 2 parameter for print, 4 parameters for write
      echo(current_dir,options);

	else if(!strcmp(cmd, "cat") && i==2) //A: Need 2 parameter for file
		cat(current_dir,options[1]);

    else if(!strcmp(cmd, "q") && i==1) //A: Quit shell
        ctrl = 0;

    else
        printf("Invalid command (type help for more info)\n");

  } while(ctrl);

  printf("Quitting...\n");
  // A: Adds inode init to the array, to avoid mem leak
  Inode* dcb = current_dir->dcb;
  add(dcbs_to_be_freed, dcb);
  free_dcbs(&disk, dcbs_to_be_freed);

  // I: Frees current block of dir handle previously allocated (NEW)
  DirectoryBlock* db = current_dir->current_block;
  if(db)
     free(db);

  free(dh_clone);
  free(current_dir);

  printf("===================================\n");
  printf("EXIT\n");
  printf("===================================\n");

  return 0;
}
