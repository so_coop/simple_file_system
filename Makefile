CCOPTS= -Wall -g -std=gnu99 -Wstrict-prototypes -Wno-missing-braces
LIBS=
CC=gcc
AR=ar


BINS = simplefs_test bitmap_test disk_driver_test shell

OBJS = bitmap.o disk_driver.o simplefs.o

HEADERS = bitmap.h\
	disk_driver.h\
	simplefs.h\
	tools.h

%.o:	%.c $(HEADERS)
	$(CC) $(CCOPTS) -c -o $@  $<

.phony: clean all

simplefs_test: simplefs_test.o $(OBJS) $(HEADERS) test.h
		$(CC) $(CCOPTS) bitmap.c disk_driver.c simplefs.c lib_simplefs_test.c simplefs_test.c -o simplefs_test

disk_driver_test: disk_driver_test.o bitmap.o disk_driver.o $(HEADERS) test.h
		$(CC) $(CCOPTS) bitmap.c disk_driver_test.c disk_driver.c -o disk_driver_test

bitmap_test: bitmap_test.o bitmap.o $(HEADERS) test.h
		$(CC) $(CCOPTS) bitmap_test.c bitmap.c -o bitmap_test

shell: $(OBJS) $(HEADERS) test.h
	   $(CC) $(CCOPTS) bitmap.c disk_driver.c simplefs.c lib_simplefs_test.c shell.c -o shell

all:	$(BINS)

clean:
	rm -rf *.o *~ *.txt *.dat $(BINS)
