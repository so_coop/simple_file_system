#include <fcntl.h>    // for flag in fd
#include <sys/stat.h> // for open()
#include <sys/mman.h> // for flag in disk driver mmap and mysinc
#include <unistd.h>   // I: for access(), close()

#include "disk_driver.h"
#include "tools.h"

// opens the file (creating it if necessary_
// allocates the necessary space on the disk
// calculates how big the bitmap should be
// if the file was new
// compiles a disk header, and fills in the bitmap of appropriate size
// with all 0 (to denote the free space);

void DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks) {
    if(disk == NULL || filename == NULL || num_blocks < 1) {
        printf("Error to init the DiskDriver: Wrong parameters\n");
        return;
    }

    int bitmap_size = num_blocks/BLOCK_SIZE_BITMAP + 1; // I: Bitmap must have at least a block

    int fd;          // I: File descriptor
    int mode = 0644; // I: Owner: read & write, Group: read, Other: read
    int res;
    int area_size = sizeof(DiskHeader) + bitmap_size + BLOCK_SIZE * num_blocks; // I: Area size to be mmapped

    if(access(filename, F_OK) != -1) {
      // I: File exists
      fd = open(filename, O_RDWR, mode);       // I: Opening file in read & write
      ERROR_HELPER(fd,"Error opening file\n"); // I: ERROR_HELPER throws error if necessary

      //A: mmap() creates a new mapping in the virtual address space of the calling process
      DiskHeader* header = (DiskHeader*) mmap(0, area_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
      if(header == MAP_FAILED) {
        close(fd);
        ERROR_HELPER(-1,"Error mapping area\n");
        return;
      }

      // I: Checks if a init, for a disk with same filename but different size, is needed
      // Assuming that it can be only extended, and not reduced, to keep data integrity
      int old_num_blocks = header->num_blocks;
      int diff = num_blocks - old_num_blocks;
      if(diff > 0) {
        // I: int posix_fallocate(int fd, off_t offset, off_t len);
        // The function posix_fallocate() ensures that disk space is allocated
        // for the file referred to by the file descriptor fd for the bytes in
        // the range starting at offset and continuing for len bytes.
        // mmap needs to use space previously allocated
        int bytes_diff = BLOCK_SIZE*diff;
        // I: Allocates only "bytes_diff" bytes after previous area size (area_size - bytes_diff)
        res = posix_fallocate(disk->fd, area_size - bytes_diff, bytes_diff);
        if(res != 0) {
          close(fd);
          ERROR_HELPER(res, "Error allocating area to mmapping\n");
          return;
        }

        // First free block remains the same
        int full_blocks = old_num_blocks - header->free_blocks;
        header->num_blocks = num_blocks;
        header->bitmap_blocks = num_blocks;
        header->bitmap_entries = bitmap_size;
        header->free_blocks = num_blocks - full_blocks;
      }

      disk->header = header;	//A: save pointers to mmap space
      disk->bitmap_data = (char*)header + sizeof(DiskHeader);

    }else {
      // I: File doesn't exist
      fd = open(filename, O_CREAT | O_RDWR | O_TRUNC, mode); // I: Creating file
      ERROR_HELPER(fd,"Error creating file\n");

      // I: int posix_fallocate(int fd, off_t offset, off_t len);
      // The function posix_fallocate() ensures that disk space is allocated
      // for the file referred to by the file descriptor fd for the bytes in
      // the range starting at offset and continuing for len bytes.
      // mmap needs to use space previously allocated
      res = posix_fallocate(fd, 0, area_size);
      if(res != 0) {
        close(fd);
        ERROR_HELPER(res, "Error allocating area to mmapping\n");
        return;
      }

      //A: mmap() creates a new mapping in the virtual address space of the calling process
      DiskHeader* header =(DiskHeader*) mmap(0, area_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
      if(header == MAP_FAILED) {
        close(fd);
        ERROR_HELPER(-1,"Error mapping area\n");
        return;
      }

      //A: Prepare header
      header->num_blocks = num_blocks;
      header->bitmap_blocks = num_blocks;
      header->bitmap_entries = bitmap_size;
      header->free_blocks = num_blocks;
      header->first_free_block = 0;

      //A: I need to prepare disk only if it's a new file
      disk->header = header;
      disk->bitmap_data = (char*) header + sizeof(DiskHeader);

      bzero(disk->bitmap_data,bitmap_size);
      // A: the bzero() function erases the data in the n bytes of the memory starting at the location
      // pointed to by s, by writing zeros (bytes containing '\0') to that area.

      //A: I fill up in the bitmap of appropriate size with all 0 (to denote the free space);
    }

    disk->fd = fd;
    //close(fd) and save;
}

// reads the block in position block_num
// returns -1 if the block is free according to the bitmap
// 0 otherwise
// I: Qui cambierei con -1 free, 0 occupied, -2 wrong parameters
int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num) {
    if(disk == NULL || dest == NULL  || block_num < 0)  return WP_ERR;

    DiskHeader* header = disk->header;
    if(!header || block_num >= header->num_blocks)  return WP_ERR;

    BitMap bitmap = new_bitmap(header->bitmap_blocks, disk->bitmap_data);
    if(!bitmap_is_valid(bitmap))  return WP_ERR;

    // I: In bitmap 0 if block is free & 1 otherwise
    int res = BitMap_getBit(&bitmap, block_num);
    if(res < 0)       return WP_ERR;
    else if(!res)     return -1; //A: check if the block is free

    void* src = disk->bitmap_data + header->bitmap_entries + block_num*BLOCK_SIZE;
    // I: Copies BLOCK_SIZE characters from memory area src to memory area dest
    memcpy(dest, src, BLOCK_SIZE);

    return 0;
}

// writes a block in position block_num, and alters the bitmap accordingly
// returns -1 if operation not possible
int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num) {
    if(disk == NULL || src == NULL || block_num < 0)    return WP_ERR;

    DiskHeader* header = disk->header;
    if(!header || block_num >= header->num_blocks)  return WP_ERR;

    if (block_num == header->first_free_block)
      header->first_free_block = DiskDriver_getFreeBlock(disk, block_num + 1);//A: update first free block

    BitMap bitmap = new_bitmap(header->bitmap_blocks, disk->bitmap_data);
    if(!bitmap_is_valid(bitmap))  return WP_ERR;

    // A: Temporarily disabled, it's possible to overwrite the data
    //if(BitMap_getBit(&bitmap,block_num))  return -1; //A: check if the block is full

    int res = BitMap_getBit(&bitmap, block_num);
    if(res < 0)       return WP_ERR;
    else if(!res) {
        BitMap_set(&bitmap, block_num, 1); //A: update value of bitmap... now is full
        header->free_blocks --;            //A: update free blocks number
    }

    void* dest = disk->bitmap_data + header->bitmap_entries + block_num*BLOCK_SIZE;
    memcpy(dest,src,BLOCK_SIZE);

    return 0;
}

// frees a block in position block_num, and alters the bitmap accordingly
// returns -1 if operation not possible
int DiskDriver_freeBlock(DiskDriver* disk, int block_num) {
    if(disk == NULL || block_num < 0)    return -1;

    DiskHeader* header = disk->header;
    if(!header || block_num >= header->num_blocks)  return -1;

    BitMap bitmap = new_bitmap(header->bitmap_blocks, disk->bitmap_data);
    if(!bitmap_is_valid(bitmap))  return -1;

    if(!BitMap_getBit(&bitmap, block_num)) return -1; //A: block is already free

    BitMap_set(&bitmap, block_num, 0);

    // I: Erasing the block
    bzero(disk->bitmap_data + header->bitmap_entries + block_num*BLOCK_SIZE, BLOCK_SIZE);

    // I: Updating first free block if necessary
    if(block_num <  header->first_free_block || header->first_free_block == -1)
      header->first_free_block = block_num;

    header->free_blocks ++;	//A: updating free blocks

    return 0;
}

// returns the first free block in the disk from position (checking the bitmap)
int DiskDriver_getFreeBlock(DiskDriver* disk, int start) {
    if(disk == NULL || start < 0 ||start > disk->header->bitmap_blocks )    return -1;

    BitMap bitmap = new_bitmap(disk->header->bitmap_blocks, disk->bitmap_data);
    if(!bitmap_is_valid(bitmap))  return -1;

    // I: Bitmap_get() do it for us :)
    return BitMap_get(&bitmap, start, 0);
}

// writes the data (flushing the mmaps)
int DiskDriver_flush(DiskDriver* disk) {
    if(disk == NULL)    return -1;

    DiskHeader* header = disk->header;
    if(!header)  return -1;

    int bitmap_size = header->num_blocks/BLOCK_SIZE_BITMAP+1;
    // I: Operation must be async to work properly
    int ret = msync(header, (size_t)sizeof(DiskHeader)+bitmap_size, MS_ASYNC);

    //A: int msync(void *addr, size_t length, int flags);
    //A: it flushes changes made to the in-core copy of a file that was mapped into memory using mmap
    // back to disk.
    // I: Without use of this call, there is no guarantee that changes are written back
    //    before munmap(2) is called.  To be more precise, the part of the file
    //    that corresponds to the memory area starting at addr and having length length is updated.

    ERROR_HELPER(ret, "Problem with sync files\n");
    return 0;
}


// =========== FUNZIONI AGGIUNTE ===========

// I: Prints the DiskDriver status
void DiskDriver_print_status(DiskDriver* disk) {
    if(disk == NULL) {
           printf("No disk\n");
           return;
    }

    DiskHeader* header = disk->header;
    if(!header) {
         printf("No header\n");
         return;
    }

    printf("Disk file descriptor: %d\n", disk->fd);
    printf("Num blocks: %d\n", header->num_blocks);
    printf("Bitmap blocks: %d\n", header->bitmap_blocks);
    printf("Bitmap entries: %d\n", header->bitmap_entries);
    printf("Free blocks: %d\n", header->free_blocks);
    printf("First free block: %d\n", header->first_free_block);
}

