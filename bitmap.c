#include "bitmap.h"
#include "tools.h"

// converts a block index to an index in the array,
// and a char that indicates the offset of the bit inside the array
BitMapEntryKey BitMap_blockToIndex(int num) {

  BitMapEntryKey new_entrykey = {0};

  if(num >= 0) {
    int entry_num;
    char bit_num;
    code(&entry_num, &bit_num, num);
    new_entrykey.entry_num = entry_num;  // I: Index
    new_entrykey.bit_num   = bit_num;    // I: Offset
  }

  return new_entrykey;
}

// converts a bit to a linear index
int BitMap_indexToBlock(int entry, uint8_t bit_num) {
    if(entry<0 || bit_num<0)   return -1;
    return decode(entry, bit_num);
}

// returns the index of the first bit having status "status"
// in the bitmap bmap, and starts looking from position start
int BitMap_get(BitMap* bmap, int start, int status) {
  if(!bmap || start<0)    return -1;

  int max = bmap->num_bits;
  if(start > max)   return -1;

  int index;
  for(index=start; index<max; index++) {
    // A: calculate status
    if(BitMap_getBit(bmap, index) == status)
      return index;
  }

  return -1;
}

// sets the bit at index pos in bmap to status
int BitMap_set(BitMap* bmap, int pos, int status) {

  if(!bmap || pos<0 || pos >= bmap->num_bits)    return -1;

  BitMapEntryKey key = BitMap_blockToIndex(pos);
  //unsigned char flag = 1<<key.bit_num; //A:I avoid having to worry about the sign bit

  // A: if status is 1
  if(status){
    return setbit(bmap->entries[key.entry_num], key.bit_num);
  }else{
    return clearbit(bmap->entries[key.entry_num], key.bit_num);
  }
}

// =========== FUNZIONI AGGIUNTE ===========

// I: Creates a new static bitmap
BitMap new_bitmap(int bitmap_blocks,char* bitmap_data) {

  BitMap new_bitmap = {0};

  if(bitmap_data && bitmap_blocks > 0) {
    new_bitmap.num_bits  = bitmap_blocks;
    new_bitmap.entries   = bitmap_data;
  }

  return new_bitmap;
}

// I: Checks if a bitmap is valid, it returns 1 on success & 0 otherwise
int bitmap_is_valid(BitMap b) {
    return b.num_bits == 0 && b.entries == 0 ? 0 : 1;
}

// I: Puts in bits & entries the new position & offset
void code(int* bits, char* entries, int n) {
  *bits = n/BLOCK_SIZE_BITMAP;
  *entries = n%BLOCK_SIZE_BITMAP;
}

// I: Returns the block index from bitmap position & offset (used in BitMap_indexToBlock)
int decode(int n, uint8_t bit_num) {
    return n*BLOCK_SIZE_BITMAP + bit_num;
}

// A: Returns the status in the byte from bit
int BitMap_getBit(BitMap* bmap,int index){
  if(index >= bmap->num_bits) return -1;
  BitMapEntryKey key = BitMap_blockToIndex(index);
  return getbit(bmap->entries[key.entry_num],key.bit_num);
}

//A: Replacing itoa which is not a standard library
//Print bitmap entry
void printbinchar(char number,int num_bits){
  int i;//flag=0;
  for (i=num_bits-1; i >= 0; --i){
    // if(!flag && getbit(number,i) ) flag=1; //A: in this way print without zeroes at the beginning
    //if(flag)
      putchar( getbit(number,i) ? '1' : '0' );//A: Writes a character to the standard output (stdout).
    }
  //putchar('\n');
}

//Print bitmap
void bitmap_print(BitMap* bitmap){
  int i,max=bitmap->num_bits/BLOCK_SIZE_BITMAP;
  //printf("le posizioni iniziano dal primo 1\n");
  //A: activating the flag in printbinchar for avoid this

  for(i=0;i<max;i++){
    printbinchar(bitmap->entries[i],BLOCK_SIZE_BITMAP);
    printf(" ");
  }
  putchar('\n');
}

