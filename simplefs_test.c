#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "disk_driver.h"
#include "simplefs.h"
#include "tools.h"
#include "test.h"

#define POINTS 58

int main(int argc, char** argv) {
  printf("===================================\n");
  printf("Starting test simpleFS\n");
  printf("===================================\n\n");

  printf("Inode size = %ld\n", sizeof(Inode));
  printf("DataBlock size = %ld\n", sizeof(DataBlock));
  printf("DirectoryBlock size = %ld\n", sizeof(DirectoryBlock));
  printf("IndexesBlock size = %ld\n", sizeof(IndexesBlock));

  printf("FileControlBlock size = %ld\n", sizeof(FileControlBlock));
  printf("DirectoryTableEntry size = %ld\n\n", sizeof(DirectoryTableEntry));

  printf("DIRECTORY_TABLE_ENTRIES_IN_A_DIR_BLOCK = %ld\n", DIRECTORY_TABLE_ENTRIES_IN_A_DIR_BLOCK);
  printf("NUM_DIRECT_POINTERS = %ld\n", NUM_DIRECT_POINTERS);
  printf("NUM_INDEXES_POINTERS = %ld\n\n", NUM_INDEXES_POINTERS);

  SimpleFS fs;
  DiskDriver disk;
  const char* filename = "SimpleFS_disk_test.dat";
  int num_blocks = 100, total = 0, res = 0; // 512

  // I: Init an array of ArrayEntry where to store dcbs when a dir is changed.
  // The array content must be freed at the end of the test, otherwise we can acceed to freed blocks.
  // To add an element, use the add func who set properly the field "to_be_closed"
  ArrayEntry dcbs_to_be_freed[DCBS_NUM] = {{0}};

  printf("Initing the disk...\n");
  // I: DiskDriver_init() throws & handles errors itself, it is a void func
  DiskDriver_init(&disk, filename, num_blocks);
  DiskDriver_flush_test(&disk);
  printf("File %s created/opened\n", filename);

  DiskDriver_print_status_test(&disk);

  //A: The init to initialize
  printf("Initing the fs...\n");
  DirectoryHandle* current_dir = SimpleFS_init(&fs, &disk, dcbs_to_be_freed);
  if(!current_dir) {
    printf("Error: fs init failed\n");
    return -1;
  }

  DiskDriver_flush_test(&disk);
  printf("Fs inited with success\n");
  DiskDriver_print_status_test(&disk);

  Inode* dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("Current directory: %s\n", dcb->fcb.name);
  if(strncmp(dcb->fcb.name, "/", 1) == 0) total++;

  // I: Creates a duplicate of current dir to use pwd
  DirectoryHandle* dh_clone = malloc(sizeof(DirectoryHandle));
  *dh_clone = *current_dir;
  printf("pwd: ");
  res = SimpleFS_pwd_test(current_dir, dh_clone, dcbs_to_be_freed);
  if(!res)  total++;

  // I: Reads content of current_dir
  SimpleFS_readDir_test(current_dir);
  // I: current_dir is empty only if this is the first exec of the test, so res cannot be evaluated
  if(!dcb->num_entries)
    printf("(Expected: empty)");
  printf("\n\n");

  // I: Creates some files in root directory
  // A: I want to create this scheme
  /* A:	root
  	      ├── "File1.txt"
  	      └── "File2.odt"
  */ // I: Edit style :)

  printf("Creating some files in root directory...\n");
  const char* fn1 = "File1.txt";
  const char* fn2 = "File2.odt";

  // I: f1 & f2 will be created only if this is the first exec of the test, so res cannot be evaluated
  FileHandle* f1 = SimpleFS_createFile_test(current_dir, fn1);
  printf("\n");
  FileHandle* f2 = SimpleFS_createFile_test(current_dir, fn2);
  printf("\n");

  //printf("Creating a not supported file...\n");
  FileHandle* f3 = SimpleFS_createFile_test(current_dir, "This is a file with too many characters, it's expected an error. Some stuff...........................................................");
  if(!f3)  total++;
  printf("(Expected: error)\n\n");

  //printf("Creating another not supported file...\n");
  f3 = SimpleFS_createFile_test(NULL, "Here it's expected another error");
  if(!f3)  total++;
  printf("(Expected: error)\n\n");

  // printf("Creating again file %s...\n", fn2);
  f3 = SimpleFS_createFile_test(current_dir, fn2); // I: Cannot create files with the same name
  if(!f3)  total++;
  printf("(Expected: error)\n");

  DiskDriver_print_status_test(&disk);

  // I: Creates some directories in root directory
  /* A:	root
          ├── Dir1
  	      ├── Dir2
  	      ├── "File1.txt"
  	      └── "File2.odt"
  */ // I: Edit style :)

  printf("Creating some directories in root directory...\n");
  char* dn1 = "Dir1";
  char* dn2 = "Dir2";

  // I: dn1 & dn2 will be created only if this is the first exec of the test, so res cannot be evaluated
  res = SimpleFS_mkDir_test(current_dir, dn1);
  printf("\n");
  res = SimpleFS_mkDir_test(current_dir, dn2);
  printf("\n");

  // printf("Creating a not supported dir...\n");
  res = SimpleFS_mkDir_test(current_dir, "This is a file with too many characters, it's expected an error. Some stuff...........................................................");
  if(res)  total++;
  printf("(Expected: error)\n\n");

  // printf("Creating another not supported dir...\n");
  res = SimpleFS_mkDir_test(NULL, "Here it's expected another error");
  if(res)  total++;
  printf("(Expected: error)\n\n");

  // printf("Creating again dir %s...\n", dn2);
  res = SimpleFS_mkDir_test(current_dir, dn2); // I: Cannot create files with the same name
  if(res)  total++;
  printf("(Expected: error)\n");

  DiskDriver_print_status_test(&disk);

  // I: Reads content of root dir
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  printf("\nDrawing tree of root directory...\n");
  res = SimpleFS_tree_test(current_dir, dcbs_to_be_freed);
  if(!res)  total++;

  printf("Score: %d/10\n",total);

  // A: Create some file in dir1 and some dir in dir2
  /* A: root
          ├── Dir1
          │    ├── "File3.doc"
          │    └── "File4.docx"
  	      ├── Dir2
          │    └── Dir3
  	      ├── "File1.txt"
  	      └── "File2.odt"
  */ // I: Edit style :)

  // A: I'm in root dir, I change to parent (expected error)
  printf("\nMoving to root parent\n");
  res = SimpleFS_changeDir_test(current_dir, "..", dcbs_to_be_freed);
  if(res<0) total++;
  printf("(Expected: error)\n\n");

  // I: Reads pwd, testing if "SimpleFS_changeDir" side effect works properly
  // Duplicates current dir to use pwd
  *dh_clone = *current_dir;
  printf("pwd: ");
  res = SimpleFS_pwd_test(current_dir, dh_clone, dcbs_to_be_freed);
  if(!res)  total++;

  // I: Reads content of current_dir, testing if "SimpleFS_changeDir" side effect works properly
  SimpleFS_readDir_test(current_dir);

  // A: I'm in root dir, I change in dir1
  printf("\nMoving to %s\n", dn1);
  res = SimpleFS_changeDir_test(current_dir, dn1, dcbs_to_be_freed);
  if(!res) total++;

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("\nCurrent directory: %s\n", dcb->fcb.name);
  if(strncmp(dcb->fcb.name, dn1, 1) == 0) total++;

  // A: Duplicates current dir to use pwd
  *dh_clone = *current_dir;
  printf("pwd: ");
  res = SimpleFS_pwd_test(current_dir, dh_clone, dcbs_to_be_freed);
  if(!res)  total++;

  // I: Reads content of Dir1
  printf("\n");
  SimpleFS_readDir_test(current_dir);

  //A: Now I can create files
  const char* fn3 = "File3.doc";
  const char* fn4 = "File4.docx";

  // I: f3 & f4 will be created only if this is the first exec of the test, so res cannot be evaluated
  printf("\nCreating some files in %s...\n", dcb->fcb.name);
  f3 = SimpleFS_createFile_test(current_dir,fn3);
  printf("\n");
  FileHandle* f4 = SimpleFS_createFile_test(current_dir,fn3);
  if(!f4) total++;
  printf("(Expected: error)\n\n");
  f4 = SimpleFS_createFile_test(current_dir,fn4);
  printf("\n");

  // I: Reads content of Dir1
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  // A: I'm in dir1, I change in dir2
  printf("\nMoving to parent\n");
  res = SimpleFS_changeDir_test(current_dir, "..", dcbs_to_be_freed);
  if(!res) total++;

  printf("\nMoving to %s\n", dn2);
  res = SimpleFS_changeDir_test(current_dir, dn2, dcbs_to_be_freed);
  if(!res) total++;

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("\nCurrent directory: %s\n", dcb->fcb.name);
  if(strcmp(dcb->fcb.name, dn2) == 0) total++;

  //A: Duplicates current dir to use pwd
  *dh_clone = *current_dir;
  printf("pwd: ");
  res = SimpleFS_pwd_test(current_dir, dh_clone, dcbs_to_be_freed);
  if(!res)  total++;
  printf("\n");

  // I: Creates a dir in Dir2
  char* dn3 = "Dir3";

  // I: dn3 will be created only if this is the first exec of the test, so res cannot be evaluated
  res = SimpleFS_mkDir_test(current_dir, dn3);

  // I: Reads content of Dir2
  printf("\n");
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  // I: Testing again SimpleFS_pwd, DEEP
  printf("\nMoving to %s\n", dn3);
  res = SimpleFS_changeDir_test(current_dir, dn3, dcbs_to_be_freed);
  if(!res) total++;

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("\nCurrent directory: %s\n", dcb->fcb.name);
  if(strncmp(dcb->fcb.name, dn3, 4) == 0) total++;

  //A: Duplicates current dir to use pwd
  *dh_clone = *current_dir;
  printf("pwd: ");
  res = SimpleFS_pwd_test(current_dir, dh_clone, dcbs_to_be_freed);
  if(!res)  total++;


  //A: Renaming Dir3
  printf("\nRenaming dir %s...\n", dn3);
  res = SimpleFS_rename_test(current_dir, dcbs_to_be_freed, dn3, "new_name");
  if(!res)  total++;

  printf("\nCurrent directory: %s\n", dcb->fcb.name);
  if(strcmp(dcb->fcb.name, "new_name") == 0) total++;

  printf("\nMoving to parent\n");
  res = SimpleFS_changeDir_test(current_dir, "..", dcbs_to_be_freed);
  if(!res) total++;

  // I: Reads content of Dir2
  printf("\n");
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  //A: Restoring Dir3 name
  printf("\nRenaming dir %s...\n", dn3);
  res = SimpleFS_rename_test(current_dir, dcbs_to_be_freed, "new_name", dn3);
  if(!res)  total++;

  // I: Reads content of Dir2
  printf("\n");
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;


  //A: I'm in dir3, I change in dir1 passing by root
  printf("\nMoving to root\n");
  res = SimpleFS_changeDir_test(current_dir, "/", dcbs_to_be_freed);
  if(!res) total++;

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  // I: Reads content of root
  printf("\n");
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  // I: Draws tree
  printf("\nDrawing tree of root directory...\n");
  res = SimpleFS_tree_test(current_dir, dcbs_to_be_freed);
  if(!res)  total++;

  printf("\nMoving to %s\n", dn1);
  res = SimpleFS_changeDir_test(current_dir, dn1, dcbs_to_be_freed);
  if(!res) total++;

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("\nCurrent directory: %s\n", dcb->fcb.name);
  if(strcmp(dcb->fcb.name, dn1) == 0)   total++;

  printf("Score: %d/36\n",total);


  //A: Write, read, open and close in file in Dir1
  /* A: root
          ├── Dir1
          │    ├── "File3.doc" (TEXT1)
          │    └── "File4.docx" (TEXT6)
  	      ├── Dir2
          │    └── Dir3
  	      ├── "File1.txt"
  	      └── "File2.odt"
  */ // I: Edit style :)

  // A: Remember before performing operations in the files, they must be opened!
  // I: If files has been created in current execution, they must not to be opened

  if(!f3) {
    printf("Opening file %s...\n", fn3);
    f3 = SimpleFS_openFile_test(current_dir, fn3);
  }

  if(!f4) {
    printf("\nOpening file %s...\n", fn4);
    f4 = SimpleFS_openFile_test(current_dir, fn4);
  }

  // I: Testing write & read in one block
  printf("\nWriting file %s...\n", fn3);
  int size_text = strlen(TEXT1);
  res = SimpleFS_write_test(f3, (char*)TEXT1, size_text);
  if(res == size_text) total++;

  printf("\nReading file %s...\n", fn3);
  // I: +1 for \0 at the end of the string
  char* out_buffer = calloc(size_text+1, sizeof(char));
  res = SimpleFS_read_test(f3, out_buffer, size_text);
  if(res == size_text){
    printf("%s\n",out_buffer);
    total++;
  }
  free(out_buffer);


  // I: Testing write & read in more blocks
  printf("\nWriting file %s...\n", fn4);
  size_text = strlen(TEXT6);
  res = SimpleFS_write_test(f4, (char*)TEXT6, size_text);
  if(res == size_text) total++;

  printf("\nReading file %s...\n", fn4);
  out_buffer = calloc(size_text+1, sizeof(char));
  res = SimpleFS_read_test(f4, out_buffer, size_text);
  if(res == size_text){
    printf("%s\n", out_buffer);
    total++;
  }
  free(out_buffer);

  // I: Overwriting fn4 in one block
  res = SimpleFS_seek_test(f4, 0);
  if(res >= 0)   total++;

  printf("\nWriting file %s...\n", fn4);
  size_text = strlen(TEXT3);
  res = SimpleFS_write_test(f4, (char*)TEXT3, size_text);
  if(res == size_text) total++;

  printf("\nReading file %s...\n", fn4);
  out_buffer = calloc(size_text+1, sizeof(char));
  res = SimpleFS_read_test(f4, out_buffer, size_text);
  if(res == size_text){
    printf("%s\n", out_buffer);
    total++;
  }
  free(out_buffer);

  // I: Overwriting fn4 in more blocks
  int pos = BLOCK_SIZE-10;
  res = SimpleFS_seek_test(f4, pos);
  if(res >= 0)   total++;

  printf("\nWriting file %s...\n", fn4);
  size_text = strlen(TEXT3);
  res = SimpleFS_write_test(f4, (char*)TEXT3, size_text);
  if(res == size_text) total++;

  printf("\nReading file %s...\n", fn4);
  out_buffer = calloc(size_text+1+pos, sizeof(char));
  res = SimpleFS_read_test(f4, out_buffer, size_text+pos);
  if(res == size_text+pos){
    printf("%s\n", out_buffer);
    total++;
  }
  free(out_buffer);

  // I: Testing write with a len greater than 2 blocks(> 508*2 BYTES)
  // testing also overwriting at the end of file
  res = SimpleFS_seek_test(f4, 0);
  if(res >= 0)   total++;

  printf("\nWriting file %s...\n", fn4);
  size_text = strlen(TEXT7);
  res = SimpleFS_write_test(f4, (char*)TEXT7, size_text);
  if(res == size_text) total++;

  printf("\nReading file %s...\n", fn4);
  out_buffer = calloc(size_text+1, sizeof(char));
  res = SimpleFS_read_test(f4, out_buffer, size_text);
  if(res == size_text){
    printf("%s\n", out_buffer);
    total++;
  }
  free(out_buffer);

  printf("Score: %d/49\n",total);

  //A: Remove file
  /* I: Edit style :)
        root
        ├── D̶i̶r̶1̶
        │   ├── ̶F̶i̶l̶e̶3̶.̶d̶o̶c̶
        │   └── F̶i̶l̶e̶4̶.̶d̶o̶c̶x̶"
        ├── Dir2
        │   └── D̶i̶r̶3̶
        ├── F̶i̶l̶e̶1̶.̶t̶x̶t̶
        └── File2.odt

   */

  DiskDriver_print_status_test(&disk);

  //A: I'm in dir1 -> I change in root
  printf("Moving to root\n");
  res = SimpleFS_changeDir_test(current_dir, "..", dcbs_to_be_freed);
  if(!res) total++;

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("Current directory: %s\n", dcb->fcb.name);
  if(strcmp(dcb->fcb.name, "/") == 0)   total++;

  printf("\nDrawing tree of root directory...\n");
  SimpleFS_tree_test(current_dir, dcbs_to_be_freed);

  // I: Reads content of root
  printf("\n");
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  // A: Case 1: remove file "File1.txt"
  printf("\nRemoving file %s...\n", fn1);
  res = SimpleFS_remove_test(current_dir, (char*) fn1, dcbs_to_be_freed);
  if(!res) total++;

  // I: Reads content of root
  printf("\n");
  res = SimpleFS_readDir_test(current_dir);
  if(!res)  total++;

  printf("\nDrawing tree of root directory...\n");
  SimpleFS_tree_test(current_dir, dcbs_to_be_freed);

  DiskDriver_print_status_test(&disk);


  // A: Case 2: remove empty dir "Dir3" in "Dir2"
  printf("\nMoving to %s...\n", dn2);
  SimpleFS_changeDir_test(current_dir, dn2, dcbs_to_be_freed);

  dcb = current_dir->dcb;
  if(!dcb) {
    printf("Error: current dir is not linked with an inode\n");
    return -1;
  }

  printf("Current directory: %s\n", dcb->fcb.name);
  if(strcmp(dcb->fcb.name, "Dir2") == 0)   total++;

  printf("\nRemoving dir %s...\n", dn3);
  res = SimpleFS_remove_test(current_dir, dn3, dcbs_to_be_freed);
  if(!res) total++;

  printf("\n");
  //SimpleFS_changeDir_test(current_dir, "/", dcbs_to_be_freed);
  res = SimpleFS_readDir_test(current_dir);
  if(!res) total++;

  SimpleFS_changeDir_test(current_dir, "/", dcbs_to_be_freed);
  printf("\nDrawing tree of root directory...\n");
  SimpleFS_tree_test(current_dir, dcbs_to_be_freed);

  DiskDriver_print_status_test(&disk);

  //A: Case 3: remove a dir not empty "Dir1"
  //SimpleFS_changeDir_test(current_dir, "/", dcbs_to_be_freed);
  printf("\nRemoving dir %s...\n", dn1);
  res = SimpleFS_remove_test(current_dir, dn1, dcbs_to_be_freed);
  if(!res) total++;

  printf("\nDrawing tree of root directory...\n");
  SimpleFS_tree_test(current_dir, dcbs_to_be_freed);

  DiskDriver_print_status_test(&disk);

  // I: Adds current dir inode to the array, to avoid mem leak
  // (This is the once that I've to add manually, all the rest handle by changeDir)
  add(dcbs_to_be_freed, dcb);

  printf("\n");

  if(f1) {
    SimpleFS_close_test(f1, fn1);
    printf("\n");
  }

  if(f2) {
    SimpleFS_close_test(f2, fn2);
    printf("\n");
  }

  SimpleFS_close_test(f3, fn3);
  printf("\n");
  SimpleFS_close_test(f4, fn4);
  printf("\n");

  free_dcbs(&disk, dcbs_to_be_freed);
  free(dh_clone);

  // I: Frees current block of dir handle previously allocated (NEW)
  DirectoryBlock* db = current_dir->current_block;

  if(db)
     free(db);

  free(current_dir);

  printf("Score: %d/%d\n", total, POINTS);
  if(total == POINTS)   printf(ANSI_COLOR_GREEN "\nTEST PASSED\n" ANSI_COLOR_RESET);
  else                  printf(ANSI_COLOR_RED "\nTEST FAILED\n" ANSI_COLOR_RESET);

  return 0;
}
