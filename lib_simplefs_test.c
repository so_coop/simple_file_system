// I: Lib of fs funcs to use them in our shell
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "disk_driver.h"
#include "simplefs.h"
#include "tools.h"
#include "test.h"

FileHandle* SimpleFS_createFile_test(DirectoryHandle* d, const char* filename) {
	printf("Creating file %s...\n", filename);
    FileHandle* f = SimpleFS_createFile(d, filename);

    if(!f) {
        printf("Error SimpleFS_createFile: existing a file with the same name %s or there are no free blocks or filename is too long\n", filename);
        return NULL;
    }

    printf("File %s created with success\n", filename);
    return f;
}

int SimpleFS_mkDir_test(DirectoryHandle* d, char* dirname) {
	printf("Creating dir %s...\n", dirname);
    int res = SimpleFS_mkDir(d, dirname);
    if(res) printf("Error SimpleFS_mkDir: unable to create dir %s\n", dirname);
    else    printf("Dir %s created with success\n", dirname);
    return res;
}

int SimpleFS_readDir_test(DirectoryHandle* d) {
    if(!d)  return -1;

    Inode* dcb = d->dcb;
    if(!dcb)    return -1;

	printf("Reading content of %s\n", dcb->fcb.name);

	//A: +1 for null terminator character
    int names_size = dcb->num_entries + 1;
    //char** names = calloc(names_size, sizeof(char*));
    DirectoryTableEntry* names = calloc(names_size, sizeof(DirectoryTableEntry));
    int res = SimpleFS_readDir(names, d);

    if(res) printf("Error SimpleFS_readDir: unable to read dir %s\n", dcb->fcb.name);
    else {
        printf("Dir %s read with success\n", dcb->fcb.name);
        SimpleFS_ls(names, names_size);
    }

 	free(names);
    return res;
}


FileHandle* SimpleFS_openFile_test(DirectoryHandle* d, const char* filename) {
    FileHandle* f = SimpleFS_openFile(d, filename);

    if(!f) {
        printf("Error SimpleFS_openFile: file %s doesn't exist or wrong parameters\n", filename);
        return NULL;
    }

    printf("File %s opened with success\n", filename);
    return f;
}

int SimpleFS_close_test(FileHandle* f, const char* filename) {
    printf("Closing file %s...\n", filename);
    int res = SimpleFS_close(f);

    if(res) printf("Error SimpleFS_close: unable to close the file\n");
    else    printf("File closed with success\n");
    return res;
}

int SimpleFS_read_test(FileHandle* f, void* data, int size) {
	//printf("Reading file...\n");
    int res = SimpleFS_read(f, data, size);

    if(!f) {
        printf("Error SimpleFS_read: FileHandle is NULL\n");
        return res;
    }

    Inode* fcb = f->fcb;
    if(!fcb)    return res;

    char* filename = fcb->fcb.name;
    if(res < 0) printf("Error SimpleFS_read: unable to read file %s\n", filename);
    else if(res == 0) printf("File %s is empty\n", filename);
    else        printf("File %s read with success\n", filename);
    return res;
}

int SimpleFS_write_test(FileHandle* f, void* data, int size) {
	//printf("Writing file...\n");
    int res = SimpleFS_write(f, data, size);

    if(!f) {
        printf("Error SimpleFS_read: FileHandle is NULL\n");
        return res;
    }

    Inode* fcb = f->fcb;
    if(!fcb)    return res;

    char* filename = fcb->fcb.name;
    if(res < 0) printf("Error SimpleFS_write: unable to write file %s\n", filename);
    else        printf("File %s written with success\n", filename);
    return res;
}

int SimpleFS_seek_test(FileHandle* f, int pos) {
	//printf("Writing file...\n");
    int res = SimpleFS_seek(f, pos);

    if(!f) {
        printf("Error SimpleFS_read: FileHandle is NULL\n");
        return res;
    }

    Inode* fcb = f->fcb;
    if(!fcb)    return res;

    char* filename = fcb->fcb.name;
    if(res < 0) printf("Error SimpleFS_seek: unable to seek file %s\n", filename);
    else        printf("File %s written with success\n", filename);
    return res;
}

int SimpleFS_changeDir_test(DirectoryHandle* d, char* dirname, ArrayEntry a[]) {
    int res = SimpleFS_changeDir(d, dirname, a);

    if(!d) {
        printf("Error SimpleFS_read: DirectoryHandle is NULL\n");
        return res;
    }

    if(res) printf("Error SimpleFS_changeDir: unable to change dir to %s\n", dirname);
    else    printf("Dir changed with success\n");
    return res;
}


int SimpleFS_remove_test(DirectoryHandle* d, char* dirname, ArrayEntry a[]) {
    /*Inode* parent = NULL;
    if(d)
        parent = d->directory;

    int res = SimpleFS_remove(d, dirname, a);

    if(parent)  SimpleFS_changeDir(d, parent->fcb.name, a);*/ // A: Moves to parent

    int res = SimpleFS_remove(d, dirname, a);
    SimpleFS_changeDir(d, "/", a); // A: Moves to root

    if(!d) {
        printf("Error SimpleFS_read: DirectoryHandle is NULL\n");
        return res;
    }

    if(res) printf("Error SimpleFS_remove\n");
    else    printf("%s removed with success\n", dirname);
    return res;
}

int SimpleFS_pwd_test(DirectoryHandle* d, DirectoryHandle* d_clone, ArrayEntry a[]) {
    int res = SimpleFS_pwd(d_clone, a);

    if(res < 0)      printf("Error SimpleFS_pwd\n");
    else {
        printf("\n");

        //printf("Set current block to null\n"); // I: Debug
        // I: pwd has called change dir that, to avoid mem leak,
        // frees current block of dir handle of previous dir
        d->current_block = NULL;
    }

    return res;
}

int SimpleFS_tree_test(DirectoryHandle* d, ArrayEntry a[]) {
    int res = SimpleFS_tree(d, a, 0, 0);

    if(res) printf("Error SimpleFS_tree\n");
    else    printf("Tree printed with success\n");
    return res;
}

int SimpleFS_rename_test(DirectoryHandle* d, ArrayEntry a[], char* src, char* dest) {
    int res = SimpleFS_rename(d, a, src, dest);

    if(res) printf("Error SimpleFS_rename\n");
    else    printf("File or dir renamed with success\n");
    return res;
}

// I: Avoid to insert duplicates
int add(ArrayEntry a[], Inode* e) {
    //printf("Adding e = %d\n", e);     // I: Debug
    if(!a || !e)  return -1;

    // I: Creates ArrayEntry
    ArrayEntry new_ae;
    new_ae.dcb = e;
    new_ae.to_be_closed = YES;

    for(int i=0; i<DCBS_NUM; i++) {
        ArrayEntry ae = a[i];
        //printf("ae = %d\n", ae);     // I: Debug

        if(!arrayEntry_is_valid(ae)) {
            //printf("YES\n");     // I: Debug
            a[i] = new_ae;
            return 0;
        }

        //printf("ae.dcb->fcb.name = %s\n", ae.dcb->fcb.name);     // I: Debug
        //printf("e->fcb.name = %s\n", e->fcb.name);     // I: Debug

        else if(ae.dcb == e)
            return -1;

        // I: Adding a different block for the same Inode
        else if(!strcmp(ae.dcb->fcb.name, e->fcb.name)) {
            //printf("NO\n");     // I: Debug
            // I: older block must not be closed, while the new yes
            a[i].to_be_closed = NO;
        }
    }

    return 0;
}

// I: Edits names of entries of the array "a", to be used when SimpleFS_renames is called,
// otherwise multiple frees on same pointer can happen.
// Returns the number of names entries edited, -1 on error
int edit_name(ArrayEntry a[], char* src, char* dest) {
    if(!a || !src || !dest || strlen(src) >= MAX_FILENAME_SIZE || strlen(dest) >= MAX_FILENAME_SIZE)
        return -1;

    int edited = 0;
    for(int i=0; i<DCBS_NUM; i++) {
        ArrayEntry ae = a[i];
        //printf("ae = %d\n", ae);     // I: Debug

        if(!arrayEntry_is_valid(ae))
            return edited;

        //printf("ae.dcb->fcb.name = %s\n", ae.dcb->fcb.name);     // I: Debug
        //printf("e->fcb.name = %s\n", e->fcb.name);     // I: Debug

        // I: Editing the name for the Inode, more entries could be updated
        else if(!strcmp(ae.dcb->fcb.name, src)) {
            strncpy(ae.dcb->fcb.name, dest, MAX_FILENAME_SIZE);
            edited++;
        }
    }

    return edited;
}

// I: Checks if an ArrayEntry is valid, it returns 1 on success & 0 otherwise
int arrayEntry_is_valid(ArrayEntry ae) {
    return ae.dcb == 0 && ae.to_be_closed == 0 ? 0 : 1;
}

// I: Frees dcbs of the array of ArrayEntries
int free_dcbs(DiskDriver* disk, ArrayEntry a[]) {
    if(!a)  return -1;

    for(int i=0; i<DCBS_NUM; i++) {
        ArrayEntry ae = a[i];
        //printf("i = %d\n",i);     // I: Debug
        //printf("ae.dcb = %d\nae.to_be_closed = %d\n",ae.dcb, ae.to_be_closed);     // I: Debug
        if(!arrayEntry_is_valid(ae))  return 0;

        Inode* dcb = ae.dcb;
        //if(ae.to_be_closed == YES)
            //close_dir(disk, dcb);
        free(dcb);
    }

    return 0;
}





// I: Got from DiskDriver test
void DiskDriver_print_status_test(DiskDriver* disk) {
  printf("\n***** Disk status *****\n");
  DiskDriver_print_status(disk);
  printf("***********************\n\n");
}

//A: Required for init, the flush is included in other operations
void DiskDriver_flush_test(DiskDriver* disk){
  // printf("\nFlushing data...\n");
  int res = DiskDriver_flush(disk);
  if(res)             printf("Error DiskDriver_flush, wrong parameters\n");
  else                printf("Data flushed\n");
}
