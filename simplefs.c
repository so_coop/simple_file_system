#include "simplefs.h"
#include "tools.h"
#include "disk_driver.h"

#define IS_DIR 1
#define IS_FILE 0

// initializes a file system on an already made disk
// returns a handle to the top level directory stored in the first block
DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk, ArrayEntry dcbs_to_be_freed[]) {
  if(!fs || !disk)  return NULL;

  Inode* dcb = malloc(sizeof(Inode));
  fs->disk = disk;

  // I: Check if there is something in the first block of the disk
  if(DiskDriver_readBlock(disk, dcb, 0) < 0){
    // I: An error occurred or there is nothing in the first block of the disk
    SimpleFS_format(fs);
    if(DiskDriver_readBlock(disk, dcb, 0) < 0) {
        free(dcb);
        ERROR_HELPER(-1,"Problem in format\n"); //A: An error *2
    }
  }

  // I: Adds current dir inode to the array, to avoid mem leak (NEW)
  add(dcbs_to_be_freed, dcb);

  DiskDriver_flush(disk);
  DirectoryHandle* dir_handle = malloc(sizeof(DirectoryHandle));
  dir_handle->sfs = fs;
  dir_handle->dcb = dcb;
  dir_handle->directory = NULL;  // I: Pointer to the parent directory (null if top level)
  dir_handle->current_block = get_first_element_in_inode_about_dir(disk, dcb); //A: first directory block
  dir_handle->pos_in_dir = 0;
  dir_handle->pos_in_block = 0;

  return dir_handle;
}

// creates the inital structures, the top level directory
// has name "/" and its control block is in the first position
// it also clears the bitmap of occupied blocks on the disk
// the current_directory_block is cached in the SimpleFS struct
// and set to the top level directory
void SimpleFS_format(SimpleFS* fs) {
    printf("Formatting the fs...\n");

    if(!fs) {
      printf("Error to format the SimpleFS: Wrong parameter\n"); //A: There are many if to prevent crashes
        return;
    }

    DiskDriver* disk = fs->disk;
    if(!disk || !disk->header) {
        printf("Error to format the SimpleFS: Wrong parameter\n");
        return;
    }

    DiskHeader* header = disk->header;
    if(!header) {
        printf("Error to format the SimpleFS: Wrong parameter\n");
        return;
    }

    char* bitmap = disk->bitmap_data;
    if(!bitmap) {
        printf("Error to format the SimpleFS: Wrong parameter\n");
        return;
    }

    // Clears the bitmap
    bzero(bitmap, header->bitmap_entries); // A: put 0 in every bytes in bitmap

	fs->disk->header->free_blocks = header->num_blocks; //A: bitmap clean... all blocks are free
    fs->disk->header->first_free_block = 0;

    FileControlBlock fcb = {
        .directory_block = -1,            // I: Root doesn't have a parent dir
        .block_in_disk   = 0,             // A: This is the first block
        .pos_in_parent   = -1,            // I: Root doesn't have a parent dir
        .name            = "/",           // A: The "root" block
        .size_in_bytes   = 0,             // A: The block is null
        .size_in_blocks  = 1,
        .is_dir          = IS_DIR         // I: 0 for file, 1 for dir
    };

    // I: Versione senza malloc
    Inode fdb_root = {
        .fcb = fcb,
        .num_entries = 0,  // A: No file/directory inside
        .internal_free_blocks = 0,
        .direct_indexes_blocks = {0}
    };

    // I: Write first block in the disk because
    // FirstDirectoryBlock is the first physical block of a directory
    if(DiskDriver_writeBlock(disk, &fdb_root, 0) == -1){
        printf("Error: impossible to format the fs, unable to write on the disk");
    }

	DiskDriver_flush(disk);
    printf("Fs formatted correctly\n");
}

// creates an empty file in the directory d
// returns null on error (file existing, no free blocks)
// an empty file consists only of a block of type FirstBlock
FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename) {
    if(!d || !filename || strlen(filename) >= MAX_FILENAME_SIZE)     return NULL;

   // I: Checks if a file with the same name exists
   if(SimpleFS_existFile(d, filename) != -1)
        return NULL;

    //printf("Doesn't exist a file with the same name in the inode\n"); // I: Debug

    SimpleFS* fs = d->sfs;
    if(!fs)     return NULL;

    DiskDriver* disk = fs->disk;
    if(!disk)     return NULL;

    Inode* fdb = d->dcb;
    if(!fdb)  return NULL;

    // I: Check if there is space in fs
    int free_block = DiskDriver_getFreeBlock(disk, 0);
    ERROR_HELPER(free_block,"No free space\n"); // I: There is no more space

    //printf("block where to write the new file on disk = %d\n", free_block); // I: Debug

    // I: Creates the file
    // I: Get the first free cell in inode's arrays in which to add the new file
    int block = get_free_block(disk, fdb);
    //printf("block where to write the new file in inode (equal to num_entries) = %d\n", block); // I: Debug
    FileControlBlock fcb = {
        .directory_block = (fdb->fcb).block_in_disk,    // I: File is in dir d
        .block_in_disk   = free_block,                  // I: Repeated position of the block on the disk
        .pos_in_parent   = block,                       // I: Position of the block in the parent directory inode
        .size_in_bytes   = 0,
        .size_in_blocks  = 1,
        .is_dir          = IS_FILE                      // I: 0 for file, 1 for dir
    };
    strncpy(fcb.name, filename, MAX_FILENAME_SIZE);

    // I: Updates entries in dir d
    int res = add_directory_entry(disk, fdb, block, free_block, filename, IS_FILE);
    if(res)     return NULL;

    Inode* ffb = calloc(1, sizeof(Inode));
    ffb->fcb = fcb;

    // I: Write block on disk in the free block previous calculated
    if(DiskDriver_writeBlock(disk, ffb, free_block) < 0) {
        // A: I don't use ERROR_HELPER for exit
        printf("Error in create file: cannot write the free block\n");
        // A: Remove the entry inserted before
		if(remove_directory_entry(disk, fdb, free_block) < 0)
		  printf("Error in remove entry\n");
        return NULL;
    }
    DiskDriver_flush(disk);

    //printf("in create file: fdb->fcb.block_in_disk = %d\n", fdb->fcb.block_in_disk); // I:Debug
    // I: Update dir inode block writing on disk
    if(DiskDriver_writeBlock(disk, fdb, fdb->fcb.block_in_disk) < 0) {
        printf("Error in create file: cannot update the directory block\n");
        // A: Remove the entry inserted before
		if(remove_directory_entry (disk, fdb, free_block) < 0)
		  printf("Error in remove entry \n");
        // A: Remove free block written before
		if(DiskDriver_freeBlock(disk,free_block) < 0)
		  printf("Error in free block \n");
		DiskDriver_flush(disk);
        return NULL;
    }
    DiskDriver_flush(disk);

    FileHandle* fh = malloc(sizeof(FileHandle));
    fh->sfs = fs;                                // I: pointer to memory file system structure
    fh->fcb = ffb;                               // I: pointer to the inode of the file
    fh->directory = fdb;                         // I: pointer to the directory where the file is stored
    fh->current_block = get_first_element_in_inode_about_file(disk, ffb);  // A: first data block
    fh->pos_in_file = 0;                                                   // I: position of the cursor

    return fh;
}

// I: Writes in the (preallocated) blocks array, the DirectoryTableEntry of all files in a directory
// I: Returns 0 on success, -1 on error
int SimpleFS_readDir(DirectoryTableEntry* names, DirectoryHandle* d) {
    if(!names || !d)    return -1;

    SimpleFS* fs = d->sfs;
    if(!fs)     return -1;

    DiskDriver* disk = fs->disk;
    if(!disk)     return -1;

    Inode* fdb = d->dcb;
    if(!fdb)    return -1;

    // I: Debug
    //printf("Start scanning inode of dir %s\n", fdb->fcb.name);

    // I: Solution without Directory table
    // Path to find filenames through structs is
    // DirectoryHandle -> Inode (FirstDirectoryBlock) -> FileControlBlock
    // Similar to SimpleFS_existFile()

    // I: Solution with DirectoryTable,
    // DirectoryHandle -> Inode (FirstDirectoryBlock) -> pointers -> DirectoryBlock -> DirectoryTableEntry

    // I: Start scanning the Inode of the directory d
    int entries = fdb->num_entries, names_idx = 0;
    //printf("Entries = %d\n", entries); // I: Debug

    for(int i=0; names_idx<entries; i++) {
        // I: Solution with DirectoryTable, less diskDriver readings, accessing only the table
        DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, fdb, i);

        // I: Checks if there is an internal free block
        if(dir_table_entry_is_valid(dte)) {
            //printf("add dte.filename = %s\n", dte.filename); // I: Debug
            names[names_idx] = dte;
            names_idx++;
        }

    }

    return 0;
}


// opens a file in the  directory d. The file should be existing
FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename){
  if (d == NULL || filename == NULL || strlen(filename)  >= MAX_FILENAME_SIZE) return NULL;

  SimpleFS* fs = d->sfs;
  if(!fs)     return NULL;

  DiskDriver* disk = fs->disk;
  if(!disk)     return NULL;

  // A: Checks if file exists and gets its corresponding block
  int block_num = SimpleFS_existFile(d, filename);
  if(block_num < 0) return NULL;

  Inode* ffb = malloc(sizeof(Inode));

  if(DiskDriver_readBlock(disk, ffb, block_num) < 0) {
    free(ffb);
    return NULL;
  }


  if (ffb->fcb.is_dir == IS_DIR) { //A: check if is a dir
    printf("Cannot open a directory as a file\n");
    free(ffb);
    return NULL;
  }

  //A: Preparation of the file handle to be returned
  FileHandle* fh = malloc(sizeof(FileHandle));
  fh->sfs = d->sfs;
  fh->fcb = ffb;
  fh->directory = d->dcb;
  fh->current_block = get_first_element_in_inode_about_file(disk, ffb); //A: first data block
  fh->pos_in_file = 0;

  return fh;
}

// I: Returns first element in file inode "ffb"
DataBlock* get_first_element_in_inode_about_file(DiskDriver* disk, Inode* ffb) {
    if(!ffb)    return NULL;

    int first_block = ffb->direct_indexes_blocks[0];
    //printf("first IndexesBlock block = %d\n", first_block); // I: Debug

    // I: Checks if block is valid
    if(first_block <= 0)
        return NULL;

    IndexesBlock ib = {0};
    // I: Retrieves IndexesBlock from disk
    if(DiskDriver_readBlock(disk, &ib, first_block) < 0)
         return NULL;

    first_block = ib.indexes[0];
    //printf("first DataBlock block = %d\n", first_block); // I: Debug

    // I: Checks if block is valid
    if(first_block <= 0)
        return NULL;

    // I: Retrieves DataBlock from disk
    DataBlock* db = malloc(sizeof(DataBlock));
    if(DiskDriver_readBlock(disk, db, first_block) < 0) {
         free(db);
         return NULL;
    }

    //printf("Allocated a DataBlock\n"); // I: Debug
    return db;
}

// I: Returns first element in file inode "ffb"
// I: Useful for "read" func to avoid to allocate memory
DataBlock get_first_element_in_inode_about_file_statically(DiskDriver* disk, Inode* ffb) {
    if(!ffb) {
        DataBlock err = {-1};
        return err;
    }

    int first_block = ffb->direct_indexes_blocks[0];
    //printf("first IndexesBlock block = %d\n", first_block); // I: Debug

    // I: Checks if block is valid
    if(first_block <= 0) {
        DataBlock err = {-1};
        return err;
    }

    IndexesBlock ib = {0};
    // I: Retrieves IndexesBlock from disk
    if(DiskDriver_readBlock(disk, &ib, first_block) < 0) {
        DataBlock err = {-1};
        return err;
    }

    first_block = ib.indexes[0];
    //printf("first DataBlock block = %d\n", first_block); // I: Debug

    // I: Checks if block is valid
    if(first_block <= 0) {
        DataBlock err = {-1};
        return err;
    }

    // I: Retrieves DataBlock from disk
    DataBlock db = {0};
    if(DiskDriver_readBlock(disk, &db, first_block) < 0)  {
        DataBlock err = {-1};
        return err;
    }

    return db;
}

// I: Returns first element in dir inode "ffb"
DirectoryBlock* get_first_element_in_inode_about_dir(DiskDriver* disk, Inode* ffb) {
    if(!ffb)    return NULL;

    int first_block = ffb->direct_indexes_blocks[0];

    // I: Checks if block is valid
    if(first_block <= 0)
        return NULL;

    // I: Retrieves IndexesBlock from disk
    IndexesBlock ib = {0};
    if(DiskDriver_readBlock(disk, &ib, first_block) < 0)
         return NULL;

    first_block = ib.indexes[0];

    // I: Checks if block is valid
    if(first_block <= 0)
        return NULL;

    // I: Retrieves DirectoryBlock from disk
    DirectoryBlock* db = malloc(sizeof(DirectoryBlock));
    if(DiskDriver_readBlock(disk, db, first_block) < 0) {
         free(db);
         return NULL;
    }

    //printf("Allocated a DirectoryBlock\n"); // I: Debug
    return db;
}

// closes a file handle (destroyes it)
int SimpleFS_close(FileHandle* f){
  if(!f)    return -1;

  Inode* fcb = f->fcb;
  if(!fcb)    return -1;

  // I: Frees current block of file handle previously allocated
  DataBlock* db = f->current_block;
  if(db)
    free(db);

  free(fcb); //A: Simple free
  free(f);
  return 0;
}

// writes in the file, at current position for size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes written
// I: Does side effect on "f"
int SimpleFS_write(FileHandle* f, void* data, int size){
  if(!f)    return -1;

  Inode* ffb = f->fcb;
  if(!ffb)    return -1;

  Inode* dir = f->directory;
  if(!dir)    return -1;

  SimpleFS* fs = f->sfs;
  if(!fs)     return -1;

  DiskDriver* disk = fs->disk;
  if(!disk)     return -1;

  int free_space, ret, pos = f->pos_in_file, written_bytes = 0; //A: position of file

  // I: Useful if a DataBlock allocation is necessary
  int free_block = -1;

  // I: "overwriting" indicates if func has to overwrite data from f->pos_in_file
  // (0 equal to false, otherwise equal to true)
  int overwriting = pos < ffb->fcb.size_in_bytes;
  //printf("overwriting = %d\n", overwriting); // A: debug

  DataBlock* db = f->current_block;

  //printf("in write: current data block = %d\n", db);    // I: Debug
  if(!db){
    //A: If this is the first time you write in the block, you must allocate it
    //printf("in write: allocating current data block\n");    // I: Debug

    // I: Check if there is space in fs
    free_block = DiskDriver_getFreeBlock(disk, 0);
    ERROR_HELPER(free_block,"No free space\n"); // I: There is no more space


    // I: Adding DataBlock as entry in file inode in right pos
    // (array starts from pos 0, while size_in_blocks from 1)
    //printf("ffb->fcb.size_in_blocks = %d\n", ffb->fcb.size_in_blocks);     // I: Debug
    DataBlock* new_db = add_file_entry(disk, ffb, ffb->fcb.size_in_blocks -1, free_block);
    if(!new_db)
        return -1;

    db = new_db;
    //printf("in write: current data block = %d\n", db);    // I: Debug
    //printf("db->block_in_file = %d\n", db->block_in_file);    // I: Debug
  }

  // A: I need to calculate free space where to write data
  // I: In a DataBlock we can store MAX_DATA_IN_DATA_BLOCK bytes, since pos_in_file might be greater
  // than MAX_DATA_IN_DATA_BLOCK, to find properly the free space in the interested block, we have to know
  // how much blocks are full by using module operation
  free_space = MAX_DATA_IN_DATA_BLOCK - (pos % MAX_DATA_IN_DATA_BLOCK);
  //printf("In write: free_space = %d\n", free_space); // I: Debug
  //printf("Size:%d\n",size); // A: debug
  //printf("Pos:%d\n",pos);   // A: debug

  //A: if space is smaller or equal to free space...
  if (size <= free_space) {

    // A: ... I can write date now
    // I: "Data" array is ever in the properly position thanks to recursive call that moves the cursor
    memcpy(db->data + (MAX_DATA_IN_DATA_BLOCK - free_space), data, size);
    //printf("db->data = %s\n", db->data); // I: Debug

    // I: Checks if a DataBlock has been allocated in this func calling
    if(free_block > 0) {
        ret = DiskDriver_writeBlock(disk, db, free_block);
        //printf("Ret: %d\n",ret);    // A: debug
        ERROR_HELPER (ret,"Problem with write\n");
        DiskDriver_flush(disk);

        // I: Updates FileHandle & corresponding inode
        f->current_block = db;
        ffb->fcb.size_in_bytes += size;
    }
    else {
        // I: DataBlock has been allocated before this func calling
        ret = DiskDriver_writeBlock(disk, db, db->block_in_disk);
        //printf("Ret: %d\n",ret);    // A: debug
        ERROR_HELPER (ret,"Problem with write\n");
        DiskDriver_flush(disk);

        // I: Updates FileHandle & corresponding inode
        // I: Checks if func has to overwrite current block
        if(overwriting)
            overwrite(ffb, db->block_in_file, free_space, size);
        // I: Else, appends
        else
            ffb->fcb.size_in_bytes += size;
    }

    // I: Update file inode block writing on disk
    // A: It is important to save the changes. This value is also used for other funcs
    if(DiskDriver_writeBlock(disk, ffb, ffb->fcb.block_in_disk) < 0) {
        printf("Error in create file: cannot update the directory block\n");
        // A: Remove free block written before
        if(free_block > 0) {
            if(DiskDriver_freeBlock(disk,free_block) < 0)
              printf("Error in free block \n");
            DiskDriver_flush(disk);
        }
        return -1;
    }
    DiskDriver_flush(disk);

    //printf("ffb->fcb.size_in_bytes =  %d\n", ffb->fcb.size_in_bytes);   //A: debug
    // I: Common updates for FileHandle & corresponding inode
    f->pos_in_file += size;
    written_bytes += size;
	//printf("Written_bytes: %d\n", written_bytes); //A: debug

    //printf("in create file: fdb->fcb.block_in_disk = %d\n", fdb->fcb.block_in_disk); // I:Debug
    // I: Update dir inode block writing on disk
    // A: It is important to save the changes. This value is also used for other funcs
    if(DiskDriver_writeBlock(disk, dir, dir->fcb.block_in_disk) < 0) {
        printf("Error in create file: cannot update the directory block\n");
        // A: Remove free block written before
		if(free_block > 0) {
            if(DiskDriver_freeBlock(disk,free_block) < 0)
              printf("Error in free block \n");
            DiskDriver_flush(disk);
        }
        return -1;
    }
    DiskDriver_flush(disk);
    return written_bytes;
  }

  else {
    // A: if space is greater than free space...

    //printf("in write: writing free_space bytes\n");    // I: Debug

    // I: Writes data in free space of the current block (No internal fragmentation)
    // I: "Data" array is ever in the properly position thanks to recursive call that moves the cursor
    memcpy(db->data + (MAX_DATA_IN_DATA_BLOCK - free_space), data, free_space);
    //printf("db->data = %s\n", db->data); // I: Debug

    // I: Writes on disk
    // I: Checks if a DataBlock has been allocated in this func calling
    if(free_block > 0) {
        ret = DiskDriver_writeBlock(disk, db, free_block);
        //printf("Ret: %d\n",ret);
        ERROR_HELPER (ret,"Problem with write\n");
        DiskDriver_flush(disk);

        // I: Updates FileHandle & corresponding inode
        ffb->fcb.size_in_bytes += free_space;
    }
    else {
        // I: DataBlock has been allocated before this func calling
        ret = DiskDriver_writeBlock(disk, db, db->block_in_disk);
        //printf("Ret: %d\n",ret);
        ERROR_HELPER (ret,"Problem with write\n");
        DiskDriver_flush(disk);

        // I: Updates FileHandle & corresponding inode
        // I: Checks if func has to overwrite current block
        if(overwriting)
            overwrite(ffb, db->block_in_file, free_space, free_space);
        // I: Else, appends
        else
            ffb->fcb.size_in_bytes += free_space;
    }

    // I: Update file inode block writing on disk
    // A: It is important to save the changes. This value is also used for other funcs
    if(DiskDriver_writeBlock(disk, ffb, ffb->fcb.block_in_disk) < 0) {
        printf("Error in create file: cannot update the directory block\n");
        // A: Remove free block written before
        if(free_block > 0) {
            if(DiskDriver_freeBlock(disk,free_block) < 0)
              printf("Error in free block \n");
            DiskDriver_flush(disk);
        }
        return -1;
    }
    DiskDriver_flush(disk);

    // A: Common updates for FileHandle and corresponding inode
    f->pos_in_file += free_space;

    // I: As for createFile, it searches the next block in file's inode DataBlock pointers
    DataBlock* next = get_entry_in_Inode_about_file(disk, ffb, db->block_in_file +1);
    f->current_block = next;
    // I: Frees previous file handler current block
    free(db);

    //printf("in create file: fdb->fcb.block_in_disk = %d\n", fdb->fcb.block_in_disk); // I:Debug
    // I: Update dir inode block writing on disk
    // A: It is important to save the changes. This value is also used for other funcs
    if(DiskDriver_writeBlock(disk, dir, dir->fcb.block_in_disk) < 0) {
        printf("Error in create file: cannot update the directory block\n");
        // A: Remove free block written before
		if(DiskDriver_freeBlock(disk,free_block) < 0)
		  printf("Error in free block \n");
		DiskDriver_flush(disk);
        return -1;
    }
    DiskDriver_flush(disk);


    //A: Remaining bytes will be written in recursive way
    int written = SimpleFS_write(f, data + free_space, size - free_space) + free_space;
    //printf("in write: total written bytes = %d\n", written);    // I: Debug
    if (written < 0)    return -1;

    return written;
  }

  return written_bytes;
}

// I: Calculates new file size when it's overwriting the file
// it's always: new file size >= old file size
// (i.e. filesize 300 bytes, write from 290° byte, 20 bytes => new size = 290+20 = 310 bytes)
// (i.e. filesize 300 bytes, write from 200° byte, 20 bytes => new size = 200+20 = 220 bytes ?
//  no, it is 300 bytes)
void overwrite(Inode* ffb, int block_in_file, int free_space, int size) {
    //printf("Overwriting file...\n"); // I: Debug
    // I: Space occupied by previous blocks (no internal frag, so they are full)
    int prev_blocks_space = block_in_file * MAX_DATA_IN_DATA_BLOCK;
    // I: Starts writing from this pos in current DataBlock
    int start = MAX_DATA_IN_DATA_BLOCK - free_space;
    //printf("db->block_in_file = %d\n", block_in_file);
    //printf("prev_blocks_space = %d\nstart = %d\n", prev_blocks_space, start);
    int new_size = prev_blocks_space + start + size;
    if(ffb->fcb.size_in_bytes < new_size)
        ffb->fcb.size_in_bytes = new_size;
}

// I: Reads the file from the first block for "size" bytes and stores the content in "data" (prev allocated).
// Returns the number of bytes read, -1 on error.
int SimpleFS_read(FileHandle* f, void* data, int size){
  if(!f || !data)    return -1;

  Inode* ffb = f->fcb;
  if(!ffb)    return -1;

  SimpleFS* fs = f->sfs;
  if(!fs)     return -1;

  DiskDriver* disk = fs->disk;
  if(!disk)     return -1;

  // I: I can use static Datablock because the func doesn't side-effect on file handle "f"
  DataBlock first = get_first_element_in_inode_about_file_statically(disk, ffb);
  DataBlock* current_block = &first;

  //int size_in_bytes = ffb->fcb.size_in_bytes;   // A: size_in_bytes -> bytes occupied
  int size_in_blocks = ffb->fcb.size_in_blocks;
  int bytes_read = 0; //, ret
  //printf("size_in_blocks %d\n",size_in_blocks); //A: debug
  //printf("size_in_bytes %d\n",size_in_bytes); //A: debug
  //printf("size: %d\n", size); //A: debug

  // I: This loop reads one block at once
  for(int i=0; i<size_in_blocks; i++) {
    //printf("current_block_num = %d\n", i); // A: Debug


    // I: In the loop current_block cannot be NULL
    if(!current_block)  return -1;

    //printf("current_block->data = %s\n", current_block->data); // I: Debug

    if(size > MAX_DATA_IN_DATA_BLOCK) {
        // I: Size exceeds limit of a DataBlock
        memcpy(data + bytes_read, current_block->data, MAX_DATA_IN_DATA_BLOCK);  //A: I read the bytes

        /*ret = DiskDriver_readBlock(disk, db, ffb->fcb.block_in_disk);
        printf("Ret: %d\n",ret);
        ERROR_HELPER (ret,"Problem with read\n");
        DiskDriver_flush(disk);*/

        bytes_read += MAX_DATA_IN_DATA_BLOCK;
        size -= MAX_DATA_IN_DATA_BLOCK;
        //printf("Bytes_read: %d\n", bytes_read); //A: debug
    }

    else {
        // I: Size doesn't exceeds limit of a DataBlock
        memcpy(data + bytes_read, current_block->data , size /*sizeof(current_block->data)*/);  //A: I read the bytes

        //printf("HO LETTO %s\n",data + bytes_read); //A: debug
        /*ret = DiskDriver_readBlock(disk, current_block, current_block->block_in_disk);
        printf("Ret: %d\n",ret);
        ERROR_HELPER (ret,"Problem with read\n");
        DiskDriver_flush(disk);*/

        bytes_read += size;
        //bytes_read += sizeof(current_block->data); //A: debug
        //printf("Bytes_read: %d\n", bytes_read); //A: debug
        return bytes_read;
    }


    // I: Accesses the next block
    DataBlock db = get_entry_in_Inode_about_file_statically(disk, ffb, i+1);
    current_block = &db;
  }

  return bytes_read;
}

// returns the number of bytes read (moving the current pointer to pos)
// returns pos on success
// -1 on error (file too short)
int SimpleFS_seek(FileHandle* f, int pos){
  if(!f)    return -1;

  Inode* ffb = f->fcb;
  if(!ffb)    return -1;

  //A: check if file is too short or the index is invalid
  if(pos < 0 || pos > ffb->fcb.size_in_bytes)   return -1;

  SimpleFS* fs = f->sfs;
  if(!fs)     return -1;

  DiskDriver* disk = fs->disk;
  if(!disk)     return -1;

  f->pos_in_file = pos;
  // I: Frees old file handle current block to avoid mem leak
  free(f->current_block);
  // I: Gets new file handle current block allocated dinamically because it return a pointer outside the
  // lifecycle of this func
  f->current_block = get_entry_in_Inode_about_file(disk, ffb, pos/MAX_DATA_IN_DATA_BLOCK);

  return pos;
}

// seeks for a directory in d. If dirname is equal to ".." it goes one level up
// 0 on success, negative value on error
// it does side effect on the provided handle
// I: The array "dcbs_to_be_freed" contains directory inode to be freed at the end of test/shell exec
// to avoid mem leaks
// I: Cannot calling free here because of maybe we can access to freed blocks
int SimpleFS_changeDir(DirectoryHandle* d, char* dirname, ArrayEntry dcbs_to_be_freed[]){
  if (d == NULL || dirname == NULL)     return -1;

  int len_dirname = strlen(dirname);
  if(len_dirname >= MAX_FILENAME_SIZE)  return -1;

  Inode* dcb = d->dcb;
  if(!dcb)     return -1;

  // I: Checks if I'm moving to current dir, so I've to do nothing
  if ((!strncmp(dcb->fcb.name, dirname, len_dirname)) || (!strncmp(dirname, ".", len_dirname)))
    return 0;

  SimpleFS* fs = d->sfs;
  if(!fs)     return -1;

  DiskDriver* disk = fs->disk;
  if(!disk)     return -1;

  // I: Checks if I'm moving to current dir parent
  if (strncmp(dirname, "..", len_dirname) == 0) {
    if (strncmp(dcb->fcb.name, "/", strlen(dcb->fcb.name)) == 0) //A: root dir doesn't have a parent
      return -1;

    // I: Current dir is not root, so moves to parent
    Inode* parent = d->directory;
    if(!parent)     return -1;

    //printf("parent = %d\n", parent); // I: Debug

    // I: Adds previous current dir inode to the array, to avoid mem leak (NEW)
    add(dcbs_to_be_freed, dcb);

    // I: Frees current block of dir handle previously allocated to avoid mem leak (NEW)
    DirectoryBlock* db = d->current_block;
    //printf("db to free = %u\n", db); // I: Debug
    if(db)
        free(db);

    d->dcb = parent;                   // I: Now current dir is the parent of the previous one
    d->directory = NULL;               // I: Current dir parent will be set later
    d->current_block = get_first_element_in_inode_about_dir(disk, parent);
    //printf("new current block = %u\n", d->current_block); // I: Debug
    d->pos_in_dir = 0;
    d->pos_in_block = 0;

    int parent_block = parent->fcb.directory_block;
    if (parent_block != -1) {
      Inode* fdb = malloc(sizeof(Inode));
      if (DiskDriver_readBlock(disk, fdb, parent_block) < 0) {
	    free(fdb);
        return -1;
      }

      d->directory = fdb;  // I: Now current dir parent is the parent of the new one

      // I: Adds current parent dir inode to the array, to avoid mem leak (NEW)
      add(dcbs_to_be_freed, fdb);
    }

    return 0;
  }

  // I: Checks if I'm moving to root
  if (strncmp(dirname, "/", len_dirname) == 0) {
    // I: Adds previous current dir inode to the array, to avoid mem leak (NEW)
    add(dcbs_to_be_freed, dcb);
    // I: Adds previous current dir parent inode to the array, to avoid mem leak (NEW)
    add(dcbs_to_be_freed, d->directory);

    // I: Frees current block of dir handle previously allocated to avoid mem leak (NEW)
    DirectoryBlock* db = d->current_block;
    //printf("db to free = %u\n", db); // I: Debug
    if(db)
        free(db);

    d->directory = NULL; // I: Root dir doesn't have a parent
    Inode* fdb = malloc(sizeof(Inode));
    // I: Root is stored in the 1st block of the disk
    if (DiskDriver_readBlock(disk, fdb, 0) < 0) {
	  free(fdb);
      return -1;
    }

    d->dcb = fdb;
    d->current_block = get_first_element_in_inode_about_dir(disk, fdb);
    //printf("new current block = %u\n", d->current_block); // I: Debug
    d->pos_in_dir = 0;
    d->pos_in_block = 0;

     // A: Adds dir inode to the array, to avoid mem leak
    add(dcbs_to_be_freed, fdb);

    return 0;
  }

  // I: I'm moving to a common dir, special cases already analyzed
  // I: Checks if dirname exists in current dir and gets its corresponding block
  int dir_block = SimpleFS_existFile(d, dirname);
  if(dir_block < 0)     return -1;

  Inode* fdb = malloc(sizeof(Inode));
  if(DiskDriver_readBlock(disk, fdb, dir_block) < 0) {
      free(fdb);
      return -1;
  }

  //A: debug
  //printf("file/dir found: %s\n",fdb.fcb.name);
  // I: Check if dirname is a dir
  if (fdb->fcb.is_dir != IS_DIR) {
	 free(fdb);
	 return -1;
   }

  // I: Adds previous current dir parent inode to the array, to avoid mem leak (NEW)
  add(dcbs_to_be_freed, d->directory);

  // AL MOMENTO QUESTO VIENE FATTO DAL MAIN ALLA FINE DEL PROGRAMMA
  // I: Adds current dir inode to the array, to avoid mem leak (NEW)
  add(dcbs_to_be_freed, fdb);

  // I: Frees current block of dir handle previously allocated to avoid mem leak (NEW)
  DirectoryBlock* db = d->current_block;
  //printf("db to free = %u\n", db); // I: Debug
  if(db)
     free(db);

  d->dcb = fdb;       // I: Now current dir is the new one
  d->directory = dcb; // I: Now current dir parent is the previous one
  d->current_block = get_first_element_in_inode_about_dir(disk, fdb);
  //printf("new current block = %u\n", d->current_block); // I: Debug
  d->pos_in_dir = 0;
  d->pos_in_block = 0;

  return 0;
}

// creates a new directory in the current one (stored in fs->current_directory_block)
// 0 on success
// -1 on error
int SimpleFS_mkDir(DirectoryHandle* d, char* dirname){
  // I: Checks if a file with the same name exists
  if(SimpleFS_existFile(d, dirname) != -1)    return -1;

  SimpleFS* fs = d->sfs;
  if(!fs)     return -1;

  DiskDriver* disk = fs->disk;
  if(!disk)     return -1;

  Inode* fdb = d->dcb;
  if(!fdb)    return -1;

  // I: Checks if there is space in fs
  int free_block = DiskDriver_getFreeBlock(disk, 0);
  ERROR_HELPER(free_block,"No free space\n"); // I: There is no more space

  // I: Get the first free cell in inode's arrays in which to add the new dir
  int block = get_free_block(disk, fdb);
  FileControlBlock fcb = {
      .directory_block = (fdb->fcb).block_in_disk,    // I: File is in dir d
      .block_in_disk   = free_block,                  // I: Repeated position of the block on the disk
      .pos_in_parent   = block,                       // I: Position of the block in the parent directory inode
      .size_in_bytes   = 0,
      .size_in_blocks  = 1,
      .is_dir          = IS_DIR                      // I: 0 for file, 1 for dir
  };
  strncpy(fcb.name, dirname, MAX_FILENAME_SIZE);

  // I: Updates entries in dir d
  int res = add_directory_entry(disk, fdb, block, free_block, dirname, IS_DIR);
  if(res < 0)   return -1;

  // I: no malloc version
  Inode ffb = {
      .fcb = fcb,
      .num_entries = 0,
      .internal_free_blocks = 0,
      .direct_indexes_blocks = {0}
  };

  // I: Write block on disk in the free block previous calculated
  if(DiskDriver_writeBlock(disk, &ffb, free_block) < 0) {
    // A: Remove the entry inserted before
		if(remove_directory_entry(disk, fdb, free_block) < 0)
		  printf("Error in remove entry \n");
    return -1;
  }
  DiskDriver_flush(disk);

  // I: Update dir inode block writing on disk
  if(DiskDriver_writeBlock(disk, fdb, fdb->fcb.block_in_disk) < 0) {
      printf("Error in mkdir: cannot update the directory block\n");
     	// A: Remove the entry inserted before
		if(remove_directory_entry(disk, fdb,free_block) < 0)
		  printf("Error in remove entry \n");
        // A: Remove free block written before
		if(DiskDriver_freeBlock(disk,free_block) < 0)
		  printf("Error in free block \n");
		DiskDriver_flush(disk);
      return -1;
  }
  DiskDriver_flush(disk);

  return 0;
}

// removes the file in the current directory
// returns -1 on failure 0 on success
// if a directory, it removes recursively all contained files
int SimpleFS_remove(DirectoryHandle* d, char* filename, ArrayEntry dcbs_to_be_freed[]) {
  //printf("Searching %s in %s\n", filename, d->dcb->fcb.name);    // A: Debug
  int block_trash = SimpleFS_existFile(d, filename);
  //printf("filename = %s\n", filename);          // A: Debug
  //printf("block_trash = %d\n", block_trash);    // A: Debug

  //A: check if the file exists
  if (block_trash < 0)  return -1;

  if(!d)    return -1;

  SimpleFS* fs = d->sfs;
  if(!fs)     return -1;

  DiskDriver* disk = fs->disk;
  if(!disk)     return -1;

  Inode* ffb = malloc(sizeof(Inode));
  if (DiskDriver_readBlock(disk, ffb , block_trash) < 0) { //A: check access file
  	free(ffb);
    ERROR_HELPER(-1, "Error in SimpleFS_remove: Unable to read the file\n");
  }

  DiskDriver_flush(disk);

  if (ffb->fcb.is_dir == IS_FILE)  //A: check if is a file or...
    SimpleFS_removeFile(d, ffb, disk);

   else {   //A: ...a dir
   	 SimpleFS_changeDir(d, ffb->fcb.name, dcbs_to_be_freed); //A: I'm entering in the directory
	 SimpleFS_removeDir(d, ffb, disk, dcbs_to_be_freed);
	//SimpleFS_changeDir(d, "..", dcbs_to_be_freed); //A Operation performed in the external library
  }

  free(ffb);
  return 0;
}


// =========== FUNZIONI AGGIUNTE ===========

// I: Check if file "filename" exists in dir "d"
// returns index on success, -1 if not exists, -2 on error
int SimpleFS_existFile(DirectoryHandle* d, const char* filename) {

    // I: Solution without Directory table
    // Path to find filename through structs is
    // DirectoryHandle -> FirstDirectoryBlock -> FileControlBlock

    // I: Solution with Directory table
    // DirectoryHandle -> Inode (FirstDirectoryBlock) -> pointers -> DirectoryBlock -> DirectoryTableEntry

    if (!d || !filename || strlen(filename) >= MAX_FILENAME_SIZE)    return -2;

    Inode* fdb = d->dcb;
    if(!fdb)    return -2;

    SimpleFS* fs = d->sfs;
    if(!fs)    return -2;

    DiskDriver* disk = fs->disk;
    if(!disk)    return -2;

    // I: Check if I'm searching current dir
    if(!strcmp(".", filename) || !strncmp(fdb->fcb.name, filename, MAX_FILENAME_SIZE))
        return fdb->fcb.block_in_disk;

    // I: Start searching the file in the inode of the directory
    int entries = fdb->num_entries, it = 0;

    // I: Qui è da vedere se nel primo blocco dobbiamo scrivere qualcosa o meno (FileControlBlock).
    // Visto che attualmente non scriviamo nulla quando creiamo un file o dir in d_ptrs[0]
    // allora dobbiamo arrivare fino a num_entries - 1.
    // Con questa implementazione il blocco zero va analizzato

    for(int i=0; it<entries; i++) {
        DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, fdb, i);

        //printf("in simpleFS_exist dte.filename = %s == %s\n", dte.filename, filename); // I: Debug

		// I: Checks if dte is an internal free block
        if(!dir_table_entry_is_valid(dte))     continue;
        it++;

        // I: Debug
        //printf("in simpleFS_exist dte->filename = %s == %s\n", dte->filename, filename);
        /*printf("in simpleFS_exist dte->inode_block = %d\n", dte->inode_block);
        printf("in simpleFS_exist dte->inode_block->fcb.filename = %s == %s\n", dte->inode_block->fcb.filename, filename);
        */

		//A: -1 for '\0' at the end of the string
        if(!strncmp(dte.filename, filename, MAX_FILENAME_SIZE )) { //A: qua c'è un problema con valgrind
            //printf("The file already exists!\n");
            //return i; // I: In this way, func returns the entry's position in the dir in which it is
            return dte.inode_block; // I: In this way, func returns the entry's block num on disk
        }

        // I: Solution without Directory table
        /*int res = SimpleFS_existFile_cmpName(disk, num_block, filename);
        if(res == 1)        return i;  // I: Found
        else if(res < 0)    return -1; // I: Err*/
    }

    return -1;
}


// I: Returns the Datablock* entry in position "block" in the Inode "fdb", -1 on error.
// This func must be call only if the Inode "fdb" is about a file
DataBlock* get_entry_in_Inode_about_file(DiskDriver* disk, Inode* fdb, int block) {
    if(!fdb)    return NULL;

    // I: Check if inode is about a file
    if(fdb->fcb.is_dir != IS_FILE)   return NULL;

    IndexesBlock ib = {{0}};
    // I: "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
    // since we start counting from 0
    int ib_block_in_file = block/BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK;
    //printf("in get_entry_in_Inode_about_file: block = %d\n", block); // I: Debug
    //printf("in get_entry_in_Inode_about_file: ib_block_in_file = %d\n", ib_block_in_file); // I: Debug

    if(block < BLOCKS_ADDRESSABLE_BY_DATA_BLOCK_DIRECT_POINTERS) {
        // I: Retrieves the correct IndexesBlock pointer from the inode
        int ib_block_in_disk = fdb->direct_indexes_blocks[ib_block_in_file];
        //printf("in get_entry_in_Inode_about_file: ib_block_in_disk = %d\n", ib_block_in_disk); // I: Debug

        // I: Checks if the IndexesBlock pointer is valid
        if(!ib_block_in_disk)    return NULL;

        // I: Else reads from the disk the IndexesBlock created in the past
        else if(DiskDriver_readBlock(disk, &ib, ib_block_in_disk) < 0) {
            printf("Error in get_entry_in_Inode_about_file: cannot read the IndexesBlock\n");
            return NULL;
        }
    }

    else {
        // I: Searching in indirect pointers
        // I: Block exceeds blocks directly addressed, getting the correct IndexesBlock from the chained list
        ib = get_indexes_block(disk, fdb, ib_block_in_file);

        // I: Checks if the IndexesBlock searched is valid
        if(!indexes_block_is_valid(ib))
            return NULL;
   }

   // I: Retrieves the correct DataBlock from the disk
   DataBlock* db = malloc(sizeof(DataBlock));

   // I: Calculates the new block position by removing block space occupied by previous indexes blocks.
   // "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
   // since we start counting from 0, it indicates also how many IndexesBlocks
   // there are before the interested block
   if(ib_block_in_file > 0)
      block = block - (BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK*ib_block_in_file);

   int db_block_in_disk = ib.indexes[block%BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK];
   //printf("in get_entry_in_Inode_about_file: db_block_in_disk = %d\n", db_block_in_disk); // I: Debug

   // I: Checks if the DataBlock pointer is valid
   if(!db_block_in_disk) {
        free(db);
        return NULL;
    }

   // I: Else reads from the disk the DataBlock created in the past
   else if(DiskDriver_readBlock(disk, db, db_block_in_disk) < 0) {
      printf("Error in get_entry_in_Inode_about_file: cannot read the DataBlock\n");
      free(db);
      return NULL;
   }

   return db;
}

// I: Returns the Datablock entry in position "block" in the Inode "fdb", -1 on error.
// This func must be call only if the Inode "fdb" is about a file
DataBlock get_entry_in_Inode_about_file_statically(DiskDriver* disk, Inode* fdb, int block) {
    if(!fdb) {
        DataBlock err = {{-1}};
        return err;
    }

    // I: Check if inode is about a file
    if(fdb->fcb.is_dir != IS_FILE) {
        DataBlock err = {{-1}};
        return err;
    }

    IndexesBlock ib = {{0}};
    // I: "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
    // since we start counting from 0
    int ib_block_in_file = block/BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK;
    //printf("in get_entry_in_Inode_about_file: block = %d\n", block); // I: Debug
    //printf("in get_entry_in_Inode_about_file: ib_block_in_file = %d\n", ib_block_in_file); // I: Debug

    if(block < BLOCKS_ADDRESSABLE_BY_DATA_BLOCK_DIRECT_POINTERS) {
        // I: Retrieves the correct IndexesBlock pointer from the inode
        int ib_block_in_disk = fdb->direct_indexes_blocks[ib_block_in_file];
        //printf("in get_entry_in_Inode_about_file: ib_block_in_disk = %d\n", ib_block_in_disk); // I: Debug

        // I: Checks if the IndexesBlock pointer is valid
        if(!ib_block_in_disk) {
            DataBlock err = {{-1}};
            return err;
        }

        // I: Else reads from the disk the IndexesBlock created in the past
        else if(DiskDriver_readBlock(disk, &ib, ib_block_in_disk) < 0) {
            printf("Error in get_entry_in_Inode_about_file: cannot read the IndexesBlock\n");
            DataBlock err = {{-1}};
            return err;
        }
    }

    else {
        // I: Searching in indirect pointers
        // I: Block exceeds blocks directly addressed, getting the correct IndexesBlock from the chained list
        ib = get_indexes_block(disk, fdb, ib_block_in_file);

        // I: Checks if the IndexesBlock searched is valid
        if(!indexes_block_is_valid(ib)) {//   return NULL;
            DataBlock err = {{-1}};
            return err;
        }
   }

   // I: Retrieves the correct DataBlock from the disk
   DataBlock db = {{0}};

   // I: Calculates the new block position by removing block space occupied by previous indexes blocks.
   // "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
   // since we start counting from 0, it indicates also how many IndexesBlocks
   // there are before the interested block
   if(ib_block_in_file > 0)
      block = block - (BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK*ib_block_in_file);

   int db_block_in_disk = ib.indexes[block%BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK];
   //printf("in get_entry_in_Inode_about_file: db_block_in_disk = %d\n", db_block_in_disk); // I: Debug

   // I: Checks if the DataBlock pointer is valid
   if(!db_block_in_disk) {
        DataBlock err = {{-1}};
        return err;
    }

   // I: Else reads from the disk the DataBlock created in the past
   else if(DiskDriver_readBlock(disk, &db, db_block_in_disk) < 0) {
      printf("Error in get_entry_in_Inode_about_file: cannot read the DataBlock\n");
      DataBlock err = {{-1}};
      return err;
   }

   return db;
}

// I: Returns the pointer to the IndexesBlock at pos "pos" in the chained list of IndexesBlocks
// of the inode "fdb".
// Pointer is returned as block in disk.
// It start to count from 0, this is consistent with block_in_file element in IndexesBlock struct.
int get_indexes_block_pointer(DiskDriver* disk, Inode* fdb, int pos) {
    if(!disk || !fdb || pos<0)   return -1;

    if(pos <= NUM_DIRECT_POINTERS)
        return fdb->direct_indexes_blocks[pos];

    IndexesBlock ib = {{0}};
    int last_direct_ib = fdb->direct_indexes_blocks[NUM_DIRECT_POINTERS-1];
    if(DiskDriver_readBlock(disk, &ib, last_direct_ib) < 0)
        return -1;

    int next_block_num = ib.next_block; // I: This is the position of the block in disk of the next element

    for(int i=NUM_DIRECT_POINTERS; i<pos-1; i++) {
        if(DiskDriver_readBlock(disk, &ib, next_block_num) < 0)
            return -1;
        next_block_num = ib.next_block;
    }

    return next_block_num;
}

// I: Returns the IndexesBlock at pos "pos" in the chained list of IndexesBlocks of the inode "fdb".
// It start to count from 0, this is consistent with block_in_file element in IndexesBlock struct.
IndexesBlock get_indexes_block(DiskDriver* disk, Inode* fdb, int pos) {
    IndexesBlock ib = {{0}};

    if(!disk || !fdb || pos<0)  return ib;

    /*if(pos <= NUM_DIRECT_POINTERS) {
        int ib_block = fdb->direct_indexes_blocks[pos];
        DiskDriver_readBlock(disk, &ib, next_block_num);
        return ib;
    }

    int last_direct_ib = fdb->direct_indexes_blocks[NUM_DIRECT_POINTERS-1];
    if(DiskDriver_readBlock(disk, &ib, last_direct_ib) < 0)
        return ib;

    int next_block_num = ib.next_block; // I: This is the position of the block in disk of the next element

    for(int i=NUM_DIRECT_POINTERS; i<pos; i++) {
        if(DiskDriver_readBlock(disk, &ib, next_block_num) < 0)
            return ib;
        next_block_num = ib.next_block;
    }

    return ib;*/

    int block = get_indexes_block_pointer(disk, fdb, pos);
    DiskDriver_readBlock(disk, &ib, block);
    return ib;
}



// I: Checks if a DirectoryTableEntry is valid, it returns 1 on success & 0 otherwise
int dir_table_entry_is_valid(DirectoryTableEntry dte) {
    return dte.inode_block <= 0 || dte.is_dir < 0 ? 0 : 1;
}

// I: Returns the entry of DirectoryTable in position "block" in the Inode "fdb", -1 on error.
// This func must be call only if the Inode "fdb" is about a directory
DirectoryTableEntry get_entry_in_DirectoryTable(DiskDriver* disk, Inode* fdb, int block) {
    if(!fdb) {
        DirectoryTableEntry err = {{-1}};
        return err;
    }

    // I: Check if inode is about a directory
    if(fdb->fcb.is_dir != IS_DIR) {
        DirectoryTableEntry err = {{-1}};
        return err;
    }

    IndexesBlock ib = {{0}};
    // I: "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
    // since we start counting from 0
    int ib_block_in_file = block/BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK;
    //printf("in get_entry_in_DirectoryTable: block = %d\n", block); // I: Debug
    //printf("in get_entry_in_DirectoryTable: ib_block_in_file = %d\n", ib_block_in_file); // I: Debug

    if(block < BLOCKS_ADDRESSABLE_BY_DIR_BLOCK_DIRECT_POINTERS) {
        // I: Retrieves the correct IndexesBlock pointer from the inode
        int ib_block_in_disk = fdb->direct_indexes_blocks[ib_block_in_file];
        //printf("in get_entry_in_DirectoryTable: ib_block_in_disk = %d\n", ib_block_in_disk); // I: Debug

        // I: Checks if the IndexesBlock pointer is valid
        if(!ib_block_in_disk) {
            DirectoryTableEntry err = {{-1}};
            return err;
        }

        // I: Else reads from the disk the IndexesBlock created in the past
        else if(DiskDriver_readBlock(disk, &ib, ib_block_in_disk) < 0) {
            printf("Error in get_entry_in_DirectoryTable: cannot read the IndexesBlock\n");
            DirectoryTableEntry err = {{-1}};
            return err;
        }
    }

    else {
        // I: Searching in indirect pointers
        // I: Block exceeds blocks directly addressed, getting the correct IndexesBlock from the chained list
        ib = get_indexes_block(disk, fdb, ib_block_in_file);

        // I: Checks if the IndexesBlock searched is valid
        if(!indexes_block_is_valid(ib)) {
            DirectoryTableEntry err = {{-1}};
            return err;
        }
   }

   // I: Retrieves the correct DirectoryBlock from the disk
   DirectoryBlock db = {{0}};

   // I: Calculates the new block position by removing block space occupied by previous indexes blocks.
   // "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
   // since we start counting from 0, it indicates also how many IndexesBlocks
   // there are before the interested block
   if(ib_block_in_file > 0)
      block = block - (BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK*ib_block_in_file);
   int db_block_in_disk = ib.indexes[block/MAX_FILE_BLOCKS_IN_DIR_BLOCK];
   //printf("in get_entry_in_DirectoryTable: db_block_in_disk = %d\n", db_block_in_disk); // I: Debug

   // I: Checks if the DirectoryBlock pointer is valid
   if(!db_block_in_disk) {
        DirectoryTableEntry err = {{-1}};
        return err;
    }

   // I: Else reads from the disk the DirectoryBlock created in the past
   else if(DiskDriver_readBlock(disk, &db, db_block_in_disk) < 0) {
      printf("Error in get_entry_in_DirectoryTable: cannot read the DirectoryBlock\n");
      DirectoryTableEntry err = {{-1}};
      return err;
   }

   //DirectoryTableEntry dte = db.file_blocks[block<MAX_FILE_BLOCKS_IN_DIR_BLOCK ? block : block%MAX_FILE_BLOCKS_IN_DIR_BLOCK];
   //printf("in get_entry_in_DirectoryTable: dte.inode_block = %d\n", dte.inode_block); // I: Debug
   //printf("in get_entry_in_DirectoryTable: dte.filename = %d\n", dte.filename); // I: Debug
   return db.file_blocks[block<MAX_FILE_BLOCKS_IN_DIR_BLOCK ? block : block%MAX_FILE_BLOCKS_IN_DIR_BLOCK];
}

// I: Returns the inode block in position "block" in the Inode "fdb", -1 on error.
// This func must be call only if the Inode "fdb" is about a directory
int get_inode_block_in_DirectoryBlock(DiskDriver* disk, Inode* fdb, int block) {
    DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, fdb, block);
    return dir_table_entry_is_valid(dte) ? dte.inode_block : -1;
}


// I: Adds the entry of DirectoryTable "dte" in position "block" in the Inode "fdb", -1 on error.
// "block_in_disk" is useful if the func needs to create and write on disk a new block (IndexesBlock or DirectoryBlock)
// so the func can search a free block from "block_in_disk", in "block_in_disk"-1 there will be the new inode
// This func must be call only if the Inode "fdb" is about a directory
int add_entry_in_DirectoryTable_by_Inode(DiskDriver* disk, Inode* fdb, int block, int block_in_disk, DirectoryTableEntry* dte) {
    if(!fdb)    return -1;

    // I: Check if inode is about a directory
    if(fdb->fcb.is_dir != IS_DIR)    return -1;

    IndexesBlock ib = {{0}};
    // I: "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
    // since we start counting from 0
    int ib_block_in_file = block/BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK;
    //printf("in add_entry_..._by_inode: block = %d\n", block); // I: Debug
    //printf("in add_entry_..._by_inode: ib_block_in_file = %d\n", ib_block_in_file); // I: Debug

    if(block < BLOCKS_ADDRESSABLE_BY_DIR_BLOCK_DIRECT_POINTERS) {
        // I: Retrieves the correct IndexesBlock pointer from the inode
        int ib_block_in_disk = fdb->direct_indexes_blocks[ib_block_in_file];
        //printf("in add_entry_..._by_inode: ib_block_in_disk = %d\n", ib_block_in_disk); // I: Debug

        // I: Checks if a new IndexesBlock has to be created
        if(!ib_block_in_disk) {
            ib = new_indexes_block(disk, fdb, block_in_disk, ib_block_in_file);
            if(!indexes_block_is_valid(ib))
                return -1;
        }

        // I: Else reads from the disk the IndexesBlock created in the past
        else if(DiskDriver_readBlock(disk, &ib, ib_block_in_disk) < 0) {
            printf("Error in add_entry_..._by_inode: cannot read the IndexesBlock\n");
            return -1;
        }
    }

    else {
        // I: Searching in indirect pointers
        // I: Block exceeds blocks directly addressed,
        // I: Getting the correct IndexesBlock from the chained list
        ib = get_indexes_block(disk, fdb, ib_block_in_file);

        // I: Checks if a new IndexesBlock has to be created
        if(!indexes_block_is_valid(ib)) {
            ib = new_indexes_block(disk, fdb, block_in_disk, ib_block_in_file);
            if(!indexes_block_is_valid(ib))
                return -1;
        }
   }

   // I: Retrieves the correct DirectoryBlock from the disk
   DirectoryBlock db = {{0}};

   // I: Calculates the new block position by removing block space occupied by previous indexes blocks.
   // "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
   // since we start counting from 0, it indicates also how many IndexesBlocks
   // there are before the interested block
   if(ib_block_in_file > 0)
      block = block - (BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK*ib_block_in_file);

   //printf("in add_entry_..._by_inode: block/MAX_FILE_BLOCKS_IN_DIR_BLOCK = %d/%d = %d\n", block, MAX_FILE_BLOCKS_IN_DIR_BLOCK, block/MAX_FILE_BLOCKS_IN_DIR_BLOCK); // I: Debug
   int db_block_in_disk = ib.indexes[block/MAX_FILE_BLOCKS_IN_DIR_BLOCK];
   //printf("in add_entry_..._by_inode: db_block_in_disk = %d\n", db_block_in_disk); // I: Debug

   // I: Checks if a new DirectoryBlock has to be created
   if(!db_block_in_disk) {
     //printf("dte->inode_block+1 == block_in_disk ? %d == %d\n", dte->inode_block+1, block_in_disk); // I: Debug
     // I: "new_dir_block()" will search a free block from pos "dte.inode_block+1" that is equal to "block_in_disk" var
     db = new_dir_block(disk, fdb, ib, block_in_disk, block);
     if(!dir_block_is_valid(db))
        return -1;

    // I: Putting the new written block in the array
    //printf("in add_entry_..._by_inode: db.block_in_disk = %d\n", db.block_in_disk); // I: Debug
    ib.indexes[block/MAX_FILE_BLOCKS_IN_DIR_BLOCK] = db.block_in_disk;

    if(DiskDriver_writeBlock(disk, &ib, ib.block_in_disk) < 0) {
         printf("Error in add_entry_..._by_inode: cannot write the IndexesBlock\n");
         // TODO: RIMUOVERE BLOCCO SCRITTO IN free_block
         return -1;
      }
    DiskDriver_flush(disk);
   }

   // I: Else reads from the disk the DirectoryBlock created in the past
   else if(DiskDriver_readBlock(disk, &db, db_block_in_disk) < 0) {
      printf("Error in add_entry_..._by_inode: cannot read the DirectoryBlock\n");
      return -1;
   }

   return add_entry_in_DirectoryTable_by_DirectoryBlock(disk, &db, block, dte);
}

// I: Checks if an IndexesBlock is valid, it returns 1 on success & 0 otherwise
int indexes_block_is_valid(IndexesBlock ib) {
    return ib.block_in_file < 0 || ib.block_in_disk < 0 ? 0 : 1;
}

// I: Creates a new static IndexesBlock in position "block_in_disk" on the disk
IndexesBlock new_indexes_block(DiskDriver* disk, Inode* fdb, int block_in_disk, int block_in_file) {
    if(!disk || !fdb) {
          IndexesBlock err = {{-1}};
          return err;
    }

    // I: Checks if there is space in fs
    int free_block = DiskDriver_getFreeBlock(disk, block_in_disk);
    //printf("Creating a new IndexesBlock in the free_block = %d\n", free_block); // I: Debug
    if(free_block < 0) {
          printf("No free space\n"); // I: There is no more space
          IndexesBlock err = {{-1}};
          return err;
    }

    int previous_block = -1;
    // I: Finds previous block
    if(block_in_file <= NUM_DIRECT_POINTERS) {
        // I: First block in file hasn't a previous block
        previous_block = !block_in_file ? -1 : fdb->direct_indexes_blocks[block_in_file-1];
    }

    else {
        previous_block = get_indexes_block_pointer(disk, fdb, block_in_file-1);
        if(previous_block < 0) {
            IndexesBlock err = {{-1}};
            return err;
        }
    }

    //printf("In new_indexes_block: previous_block = %d\n", previous_block); // I: Debug

    IndexesBlock ib = {
       .previous_block = previous_block,
       .indexes = {0},
       .next_block = -1,
       .block_in_file = block_in_file,
       .block_in_disk = free_block
    };

   // I: Writes the IndexesBlock on disk
   if(DiskDriver_writeBlock(disk, &ib, free_block) < 0) {
       printf("Error in new_indexes_block: cannot write the free block\n");
       IndexesBlock err = {{-1}};
       return err;
   }
   DiskDriver_flush(disk);

   // I: Updates previous blocks if it exists
   if(previous_block != -1) {
      IndexesBlock prev_ib = {{0}};
      if(DiskDriver_readBlock(disk, &prev_ib, previous_block) < 0) {
          printf("Error in new_indexes_block: cannot read the previous block\n");
          DiskDriver_freeBlock(disk, free_block);
          IndexesBlock err = {{-1}};
          return err;
      }

      prev_ib.next_block = free_block;

      if(DiskDriver_writeBlock(disk, &ib, previous_block) < 0) {
         printf("Error in new_indexes_block: cannot write the previous block\n");
         DiskDriver_freeBlock(disk, free_block);
         IndexesBlock err = {{-1}};
         return err;
      }
      DiskDriver_flush(disk);
   }

   // I: Updates Inode direct indexes blocks if necessary
   if(block_in_file <= NUM_DIRECT_POINTERS) {
       fdb->direct_indexes_blocks[block_in_file] = free_block;

       if(DiskDriver_writeBlock(disk, fdb, fdb->fcb.block_in_disk) < 0) {
         printf("Error in new_indexes_block: cannot write the inode block\n");
         DiskDriver_freeBlock(disk, free_block);
         IndexesBlock err = {{-1}};
         return err;
      }
      DiskDriver_flush(disk);
   }

   return ib;
}

// I: Creates a new static IndexesBlock for internal free blocks
IndexesBlock new_indexes_block_of_internal_free_blocks(DiskDriver* disk, Inode* fdb, int block_in_file, int previous_block) {
    if(!disk || !fdb) {
          IndexesBlock err = {{-1}};
          return err;
    }

    // I: Checks if there is space in fs
    int free_block = DiskDriver_getFreeBlock(disk, 1);
    //printf("Creating a new IndexesBlock in the free_block = %d\n", free_block); // I: Debug
    if(free_block < 0) {
          printf("No free space\n"); // I: There is no more space
          IndexesBlock err = {{-1}};
          return err;
    }

    //printf("In new_indexes_block: previous_block = %d\n", previous_block); // I: Debug

    IndexesBlock ib = {
       .previous_block = previous_block,
       .indexes = {0},
       .next_block = -1,
       .block_in_file = block_in_file,
       .block_in_disk = free_block
    };

   // I: Writes the IndexesBlock on disk
   if(DiskDriver_writeBlock(disk, &ib, free_block) < 0) {
       printf("Error in new_indexes_block: cannot write the free block\n");
       IndexesBlock err = {{-1}};
       return err;
   }
   DiskDriver_flush(disk);

   // I: Updates previous blocks if it exists
   if(previous_block != -1) {
      IndexesBlock prev_ib = {{0}};
      if(DiskDriver_readBlock(disk, &prev_ib, previous_block) < 0) {
          printf("Error in new_indexes_block: cannot read the previous block\n");
          IndexesBlock err = {{-1}};
          return err;
      }

      prev_ib.next_block = free_block;

      if(DiskDriver_writeBlock(disk, &ib, previous_block) < 0) {
         printf("Error in new_indexes_block: cannot write the previous block\n");
         DiskDriver_freeBlock(disk, free_block);
         IndexesBlock err = {{-1}};
         return err;
      }
      DiskDriver_flush(disk);
   }

   // I: Updates Inode if necessary
   if(!block_in_file) {
       fdb->internal_free_blocks = free_block;

       if(DiskDriver_writeBlock(disk, fdb, fdb->fcb.block_in_disk) < 0) {
         printf("Error in new_indexes_block: cannot write the inode block\n");
         DiskDriver_freeBlock(disk, free_block);
         IndexesBlock err = {{-1}};
         return err;
      }
      DiskDriver_flush(disk);
   }

   return ib;
}


// I: Creates a new static DirectoryBlock in position "block_in_disk" on the disk
DirectoryBlock new_dir_block(DiskDriver* disk, Inode* fdb, IndexesBlock ib, int block_in_disk, int block_in_file) {

    if(!disk || !fdb) {
       DirectoryBlock err = {{-1}};
       return err;
   }

    int free_block = DiskDriver_getFreeBlock(disk, block_in_disk);
    //printf("Creating a new DirectoryBlock in the free_block = %d\n", free_block);
    if(free_block < 0) {
       printf("No free space\n"); // I: There is no more space
       DirectoryBlock err = {{-1}};
       return err;
    }

    DirectoryBlock db = {
        .block_in_file = block_in_file,
        .block_in_disk = free_block
    };

    // I: Write DirectoryBlock on disk in the free block previous calculated
    if(DiskDriver_writeBlock(disk, &db, free_block) < 0) {
        printf("Error in add entry in dir table: cannot write the free block\n");
        DirectoryBlock err = {{-1}};
        return err;
    }
    DiskDriver_flush(disk);

    return db;
}

// I: Checks if a DirectoryBlock is valid, it returns 1 on success & 0 otherwise
int dir_block_is_valid(DirectoryBlock db) {
    return db.block_in_file < 0 || db.block_in_disk < 0 ? 0 : 1;
}


// I: Adds the entry of DirectoryTable "dte" in position "block" in the DirectoryBlock "db"
// This is an aux func for add_entry_in_DirectoryTable_by_Inode
// Returns 0 on success, -1 on error
int add_entry_in_DirectoryTable_by_DirectoryBlock(DiskDriver* disk, DirectoryBlock* db, int block, DirectoryTableEntry* dte) {
    if(!disk || !db || !dte)    return -1;

    // I: Finding offset in the file blocks array
    db->file_blocks[block<MAX_FILE_BLOCKS_IN_DIR_BLOCK ? block : block%MAX_FILE_BLOCKS_IN_DIR_BLOCK] = *dte;

    // I: Updates DirectoryBlock on disk
    if(DiskDriver_writeBlock(disk, db, db->block_in_disk) < 0) {
        printf("Error in add entry in dir table: cannot write the DirectoryBlock\n");
        return -1;
    }
    DiskDriver_flush(disk);

    return 0;
}

// I: Creates a DirectoryTableEntry in the inode "fdb" in cell "block" of the arrays.
// The new DirectoryTableEntry is init with "block_in_disk" & "filename".
// Returns 0 on success, -1 on error.
// This func must be call only if the Inode "fdb" is about a directory
int add_directory_entry(DiskDriver* disk, Inode* fdb, int block_in_dir, int block_in_disk, const char* filename, int type) {
    if(!disk)   return -1;

    // I: Versione senza malloc
    DirectoryTableEntry dte;
    dte.inode_block = block_in_disk;
    dte.is_dir = type;
    //printf("in add_dir_entry dte.inode_block (equal to pos on disk) = %d\n", dte.inode_block);
    //A: -1 for '\0' at the end of the string
    strncpy(dte.filename, filename, strlen(filename)+1);

    // I: When I add the new dte, if I need to allocate a IndexesBlock or a DirectoryBlock and write it on disk,
    // I have to search a free block from block_in_disk+1 because in "block_in_disk" I will put the
    // new file or dir
    int res = add_entry_in_DirectoryTable_by_Inode(disk, fdb, block_in_dir, block_in_disk+1, &dte);
    if(res) {
        return -1;
    }

    fdb->num_entries ++;
    //printf("fdb->num_entries = %d\n", fdb->num_entries); // I: Debug
    fdb->fcb.size_in_bytes += BLOCK_SIZE;
    fdb->fcb.size_in_blocks ++;
    return 0;
}

// I: Removes the DirectoryTableEntry in the inode "fdb" in cell "block" of the arrays.
// Returns 0 on success, -1 on error.
// This func must be call only if the Inode "fdb" is about a directory
int remove_directory_entry(DiskDriver* disk, Inode* fdb, int block) {
    int res = remove_entry_in_DirectoryTable_by_Inode(disk, fdb, block);
    if(res)     return -1;

    fdb->num_entries --;
    //printf("fdb->num_entries = %d\n", fdb->num_entries); // I: Debug
    fdb->fcb.size_in_bytes -= BLOCK_SIZE;
    fdb->fcb.size_in_blocks --;
    return 0;
}



// I: Removes the entry of DirectoryTable in position "block" in the Inode "fdb", -1 on error.
// This func must be call only if the Inode "fdb" is about a directory
int remove_entry_in_DirectoryTable_by_Inode(DiskDriver* disk, Inode* fdb, int block) {
    if(!fdb)    return -1;

    // I: Check if inode is about a directory
    if(fdb->fcb.is_dir != IS_DIR)    return -1;

    IndexesBlock ib = {{0}};
    // I: "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
    // since we start counting from 0
    int ib_block_in_file = block/BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK;
    //printf("in remove_entry_in_DirectoryTable_by_Inode: block = %d\n", block); // I: Debug
    //printf("in remove_entry_in_DirectoryTable_by_Inode: ib_block_in_file = %d\n", ib_block_in_file); // I: Debug

    if(block < BLOCKS_ADDRESSABLE_BY_DIR_BLOCK_DIRECT_POINTERS) {
        // I: Retrieves the correct IndexesBlock pointer from the inode
        int ib_block_in_disk = fdb->direct_indexes_blocks[ib_block_in_file];
        //printf("in remove_entry_in_DirectoryTable_by_Inode: ib_block_in_disk = %d\n", ib_block_in_disk); // I: Debug

        // I: Checks if the IndexesBlock pointer is valid
        if(!ib_block_in_disk)   return -1;

        // I: Else reads from the disk the IndexesBlock created in the past
        else if(DiskDriver_readBlock(disk, &ib, ib_block_in_disk) < 0) {
            printf("Error in remove_entry_in_DirectoryTable_by_Inode: cannot read the IndexesBlock\n");
            return -1;
        }
    }

    else {
        // I: Searching in indirect pointers
        // I: Block exceeds blocks directly addressed, getting the correct IndexesBlock from the chained list
        ib = get_indexes_block(disk, fdb, ib_block_in_file);

        // I: Checks if the IndexesBlock searched is valid
        if(!indexes_block_is_valid(ib))    return -1;
   }

   // I: Retrieves the correct DirectoryBlock from the disk
   DirectoryBlock db = {{0}};

   // I: Calculates the new block position by removing block space occupied by previous indexes blocks.
   // "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
   // since we start counting from 0, it indicates also how many IndexesBlocks
   // there are before the interested block
   if(ib_block_in_file > 0)
      block = block - (BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK*ib_block_in_file);

   int db_block_in_disk = ib.indexes[block/MAX_FILE_BLOCKS_IN_DIR_BLOCK];
   //printf("in remove_entry_in_DirectoryTable_by_Inode: db_block_in_disk = %d\n", db_block_in_disk); // I: Debug

   // I: Checks if the DirectoryBlock pointer is valid
   if(!db_block_in_disk)   return -1;

   // I: Else reads from the disk the DirectoryBlock created in the past
   else if(DiskDriver_readBlock(disk, &db, db_block_in_disk) < 0) {
      printf("Error in remove_entry_in_DirectoryTable_by_Inode: cannot read the DirectoryBlock\n");
      return -1;
   }

   // return remove_entry_in_DirectoryTable_by_DirectoryBlock(disk, fdb, &db, block);

   DirectoryTableEntry dte = {{0}};
   db.file_blocks[block<MAX_FILE_BLOCKS_IN_DIR_BLOCK ? block : block%MAX_FILE_BLOCKS_IN_DIR_BLOCK] = dte;

   // I: Updates DirectoryBlock on disk
   if(DiskDriver_writeBlock(disk, &db, db_block_in_disk) < 0) {
      printf("Error in remove_entry_in_DirectoryTable_by_Inode: cannot write the DirectoryBlock\n");
      return -1;
   }
   DiskDriver_flush(disk);

   return 0;
}

// I: Adds the Datablock entry "ndb" in position "block" in the Inode "ffb", -1 on error.
// "block_in_disk" is useful if the func needs to create and write on disk a new block (IndexesBlock or DataBlock)
// so the func can search a free block from "block_in_disk", in "block_in_disk"-1 there will be the new inode
// This func must be call only if the Inode "ffb" is about a file
// Returns 0 on success, -1 on error
int add_DataBlock_entry_in_Inode(DiskDriver* disk, Inode* ffb, int block, int block_in_disk, DataBlock* ndb) {
    if(!ffb)    return -1;

    // I: Check if inode is about a file
    if(ffb->fcb.is_dir != IS_FILE)    return -1;

    IndexesBlock ib = {{0}};
    // I: "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
    // since we start counting from 0
    int ib_block_in_file = block/BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK;
    //printf("in add_DataBlock_entry_in_Inode: block = %d\n", block); // I: Debug
    //printf("in add_DataBlock_entry_in_Inode: ib_block_in_file = %d\n", ib_block_in_file); // I: Debug

    if(block < BLOCKS_ADDRESSABLE_BY_DATA_BLOCK_DIRECT_POINTERS) {
        // I: Retrieves the correct IndexesBlock pointer from the inode
        int ib_block_in_disk = ffb->direct_indexes_blocks[ib_block_in_file];
        //printf("in add_DataBlock_entry_in_Inode: ib_block_in_disk = %d\n", ib_block_in_disk); // I: Debug

        // I: Checks if a new IndexesBlock has to be created
        if(!ib_block_in_disk) {
            ib = new_indexes_block(disk, ffb, block_in_disk, ib_block_in_file);
            if(!indexes_block_is_valid(ib))
                return -1;
        }

        // I: Else reads from the disk the IndexesBlock created in the past
        else if(DiskDriver_readBlock(disk, &ib, ib_block_in_disk) < 0) {
            printf("Error in add_DataBlock_entry_in_Inode: cannot read the IndexesBlock\n");
            return -1;
        }
    }

    else {
        // I: Searching in indirect pointers
        // I: Block exceeds blocks directly addressed,
        // I: Getting the correct IndexesBlock from the chained list
        ib = get_indexes_block(disk, ffb, ib_block_in_file);

        // I: Checks if a new IndexesBlock has to be created
        if(!indexes_block_is_valid(ib)) {
            ib = new_indexes_block(disk, ffb, block_in_disk, ib_block_in_file);
            if(!indexes_block_is_valid(ib))
                return -1;
        }
   }

   // I: Calculates the new block position by removing block space occupied by previous indexes blocks.
   // "ib_block_in_file" indicates which position I've to access in the chained list of IndexesBlocks,
   // since we start counting from 0, it indicates also how many IndexesBlocks
   // there are before the interested block
   if(ib_block_in_file > 0)
      block = block - (BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK*ib_block_in_file);

   // I: Putting the new DataBlock pointer in the array
   //printf("in add_DataBlock_entry_in_Inode: block%BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK = %dmod%d = %d\n", block, BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK, block%BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK); // I: Debug
   ib.indexes[block%BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK] = ndb->block_in_disk;

   // I: Updates the IndexesBlock on disk
   if(DiskDriver_writeBlock(disk, &ib, ib.block_in_disk) < 0) {
        printf("Error in add_DataBlock_entry_in_Inode: cannot write the inode\n");
        return -1;
   }
   DiskDriver_flush(disk);

   return 0;
}

// I: Creates a new dynamic DataBlock in position "block_in_disk" or successive on the disk
DataBlock* new_data_block(DiskDriver* disk, int block_in_disk, int block_in_file) {
    if(!disk) {
       return NULL;
   }

    int free_block = DiskDriver_getFreeBlock(disk, block_in_disk);
    //printf("Creating a new DataBlock in the free_block = %d\n", free_block);
    if(free_block < 0) {
       printf("No free space\n"); // I: There is no more space
       return NULL;
    }

    // A: Dynamic allocation to solve valgrind problems
    DataBlock* db = calloc(1, sizeof(DataBlock));
    db->block_in_file = block_in_file;
    db->block_in_disk = block_in_disk;

    // I: Write DirectoryBlock on disk in the free block previous calculated
    if(DiskDriver_writeBlock(disk, db, free_block) < 0) {
        printf("Error in new_data_block: cannot write the free block\n");
        free(db);
        return NULL;
    }
    DiskDriver_flush(disk);

    return db;
}

// I: Checks if a DataBlock is valid, it returns 1 on success & 0 otherwise
int data_block_is_valid(DataBlock db) {
    return db.block_in_file < 0 || db.block_in_disk < 0 ? 0 : 1;
}

// I: Creates a Datablock in the inode "ffb" in cell "block" of the arrays.
// Returns the new Datablock on success, NULL on error.
// This func must be call only if the Inode "fdb" is about a file
DataBlock* add_file_entry(DiskDriver* disk, Inode* ffb,  int block_in_file, int block_in_disk) {
    // I: Creates a new Datablock
    DataBlock* db = new_data_block(disk, block_in_disk, block_in_file);
    if(!db)     return NULL;

    // I: Puts the new Datablock in the inode "ffb"
    int res = add_DataBlock_entry_in_Inode(disk, ffb, block_in_file, block_in_disk, db);
    if(res) {
        free(db);
        return NULL;
    }

    ffb->fcb.size_in_blocks ++;
    //printf("Updating ffb->fcb.size_in_blocks = %d\n", ffb->fcb.size_in_blocks); // I: Debug
    return db;
}

// I: Adds the entry "entry" in the Inode "ffb" in internal free blocks, -1 on error.
// This func must be call only if the Inode "ffb" is about a dir
// Returns 0 on success, -1 on error
int add_entry_in_internal_free_blocks(DiskDriver* disk, Inode* ffb, int entry) {
    if(!ffb)    return -1;

    // I: Check if inode is about a dir
    if(ffb->fcb.is_dir != IS_DIR)    return -1;

    IndexesBlock ib = {{0}};
    int ifb = ffb->internal_free_blocks;

    //printf("In add_entry_in_internal_free_blocks: entry = %d\n", entry);

    // I: Checks if a new IndexesBlock has to be created
    if(!ifb) {
        ib = new_indexes_block_of_internal_free_blocks(disk, ffb, 0, -1);
        if(!indexes_block_is_valid(ib))
            return -1;

        ffb->internal_free_blocks = ib.block_in_disk;

        // I: Updates the inode on disk
        if(DiskDriver_writeBlock(disk, ffb, ffb->fcb.block_in_disk) < 0) {
            printf("Error in add_entry_in_internal_free_blocks: cannot write the inode\n");
            return -1;
        }
        DiskDriver_flush(disk);
    }

    // I: Else reads from the disk the IndexesBlock created in the past
    else if(DiskDriver_readBlock(disk, &ib, ifb) < 0) {
        printf("Error in add_entry_in_internal_free_blocks: cannot read the IndexesBlock\n");
        return -1;
    }

    int i = 0, pos = 0, previous_block = -1;
    // I: Finds a free block in the indexes blocks of internal free blocks
    while(ib.indexes[i]) {
        // I: Finds a free block in the current indexes block of internal free blocks
        for(i=0; i<NUM_INDEXES_POINTERS; i++) {
            if(!ib.indexes[i])
                break;
        }

        // I: Checks if a free block has been found
        if(!ib.indexes[i])
                break;

        // I: Accesses the next Indexes block of internal free blocks
        previous_block = ifb;
        ifb = ib.next_block;
        pos++;

        // I: Checks if a new IndexesBlock has to be created
        if(!ifb) {
            ib = new_indexes_block_of_internal_free_blocks(disk, ffb, pos, previous_block);
            if(!indexes_block_is_valid(ib))
                return -1;
        }

        else if(DiskDriver_readBlock(disk, &ib, ifb) < 0) {
            printf("Error in add_entry_in_internal_free_blocks: cannot read the IndexesBlock\n");
            return -1;
        }
   }

   ib.indexes[i] = entry;
   //printf("ffb->internal_free_blocks = %d\n", ffb->internal_free_blocks);  // I: Debug
   //printf("ib.indexes[%d] = %d\n", i, ib.indexes[i]);  // I: Debug

   // I: Updates the IndexesBlock on disk
   if(DiskDriver_writeBlock(disk, &ib, ib.block_in_disk) < 0) {
        printf("Error in add_entry_in_internal_free_blocks: cannot write the IndexesBlock\n");
        return -1;
   }
   DiskDriver_flush(disk);

   return 0;
}

// I: Removes the linked list of internal free blocks of the inode
// Returns 0 on success, -1 on error
int remove_internal_free_blocks(DiskDriver* disk, Inode* dir) {
    if(!disk || !dir)    return -1;

    int ifb = dir->internal_free_blocks;
    if(!ifb)    return 0;        // I: There isn't an internal free block available
    else {
        // I: There is an internal free block available
        IndexesBlock ib = {0};

        while(ifb > 0) {
            //printf("ifb = %d\n", ifb); // I: Debug
            // A: Retrieves IndexesBlock from disk
            if(DiskDriver_readBlock(disk, &ib, ifb) < 0) {
                printf("Error in get_free_block: Unable to read IndexesBlock of internal free blocks\n");
                 return -1;
             }

            // A: Accesses the next Indexes block of internal free blocks
            int previous_block = ifb;
            ifb = ib.next_block;

            // I: Removes current IndexesBlock block from the disk
            DiskDriver_freeBlock(disk, previous_block);
        }
    }

    return -1;
}

// I: Returns a free block pos in dir table of inode "dir", checking before if there is an internal
// free block available, -1 on error
int get_free_block(DiskDriver* disk, Inode* dir) {
    if(!disk || !dir)    return -1;

    int ifb = dir->internal_free_blocks;
    if(!ifb)    return dir->num_entries;        // I: There isn't an internal free block available
    else {
        // I: There is an internal free block available
        IndexesBlock ib = {0};

        while(ifb > 0) {
            //printf("ifb = %d\n", ifb); // I: Debug
            // A: Retrieves IndexesBlock from disk
            if(DiskDriver_readBlock(disk, &ib, ifb) < 0) {
                printf("Error in get_free_block: Unable to read IndexesBlock of internal free blocks\n");
                 return -1;
             }

            // A: Finds a valid block in current IndexesBlock, returning the first
            for(int i=0; i<NUM_INDEXES_POINTERS; i++) {
                int first_block = ib.indexes[i];
                //printf("first_block-1 = %d\n", first_block-1);  // I: Debug
                if(first_block > 0) {
                    // I: Updates inode and returns the correct free block position in the inode
                    ib.indexes[i] = 0;

                    if(DiskDriver_writeBlock(disk, &ib, ib.block_in_disk) < 0) {
                        printf("Error in get_free_block: Unable to write inode\n");
                        return -1;
                    }

                    return first_block-1;       // AGGIUNGO POS IN PARENT +1 PER DISTINGUERE DALLA INIT CON TUTTI ZERI
                }
            }

            // A: Accesses the next Indexes block of internal free blocks
            int previous_block = ifb;
            ifb = ib.next_block;

            // A: If a valid block is not found in current IndexesBlock,
            // remove its block from the disk
            DiskDriver_freeBlock(disk, previous_block);
        }

        // I: If a valid block is not found in all IndexesBlocks,
        // this means that all past internal free blocks have been used,
        // I: Updates inode and returns the correct free block position in the inode
        dir->internal_free_blocks = 0;

        if(DiskDriver_writeBlock(disk, dir, dir->fcb.block_in_disk) < 0) {
            printf("Error in get_free_block: Unable to write inode\n");
            return -1;
        }

        return dir->num_entries;
    }

    return -1;
}

// I: Returns if "block" is an internal block in dir table of inode "dir", -1 on error
int is_an_internal_block(DiskDriver* disk, Inode* dir, int block) {
    if(!dir)    return -1;
    return (dir->num_entries -1) != block;
    // I: If equals, block doesn't create a "hole" by removing it, because it would be at the end of the dir table
}

// A: Remove file from disk
//	 Returns 0 on success, -1 on error
// I: Call this after SimpleFS_close(FileHandle* f), "ffb" is the file inode
int SimpleFS_removeFile(DirectoryHandle* d, Inode* ffb, DiskDriver* disk){
   if(!d || !ffb || !disk)  return -1;

  //A: Check index
  // int index =SimpleFS_existFile(d->sfs->disk, ffb->fcb.name);
  // ERROR_HELPER(index,"File doesn't exist\n");
  //A: Get position of file in the directory
  //A: Check all pointer

  //IndexesBlock ib = {{0}};
  int i, ret, block_trash, size_in_block = ffb->fcb.size_in_blocks;
  //printf("final_block = %d\n", size_in_block);

  //A: Get the first element and setting the current...
  //DataBlock first = get_first_element_in_inode_about_file_statically(disk, ffb);
  //DataBlock* db = f->current_block;

  //A: This loop reads one block at once of the file
  for(i=0; i<size_in_block; i++){

	//A: get inode_block checks all indirect pointers
    DataBlock db = get_entry_in_Inode_about_file_statically(disk, ffb, i);
    if(!data_block_is_valid(db))  continue;
    // I: block size è 1 quando viene creato un file,
    // anche se il fcb non viene scritto su disco come blocco a parte rispetto all'inode

    block_trash = db.block_in_disk;
    //printf("block_trash = %d\n", block_trash);
    // I: Checks if the block is valid (in 0 there is the root of fs)
    if(block_trash > 0) {
	  	//A: Free block in disk
  		ret = DiskDriver_freeBlock(disk, block_trash);
  		ERROR_HELPER(ret,"Error in remove file: Unable to free a data block\n");
  		DiskDriver_flush(disk);
    }

  }

  int pos_in_parent = ffb->fcb.pos_in_parent;
  //printf("ffb->fcb.block_in_disk = %d\n", ffb->fcb.block_in_disk);
  //printf("ffb->fcb.pos_in_parent = %d\n", ffb->fcb.pos_in_parent);
  //A: Free block in disk
  ret = DiskDriver_freeBlock(disk, ffb->fcb.block_in_disk);
  ERROR_HELPER(ret,"Error in remove file: Unable to free the inode block\n");
  DiskDriver_flush(disk);

  //A: Update parent
  Inode* ffb_parent = d->dcb;
  remove_directory_entry(disk, ffb_parent, pos_in_parent);

  if(is_an_internal_block(disk, ffb_parent, pos_in_parent)) {
        // I: pos_in_parent+1 to distinguish element in the array indexes from the init with zeros.
        // Remember: first block in a dir has pos in parent equals to zero
        ret = add_entry_in_internal_free_blocks(disk, ffb_parent, pos_in_parent+1);
        if(ret < 0) {
            return -1;
        }
  }

  // I: Writes the updates on disk
  ret = DiskDriver_writeBlock(disk, ffb_parent, ffb_parent->fcb.block_in_disk);
  ERROR_HELPER(ret,"Error in remove file: Unable to write the dir inode block\n");
  DiskDriver_flush(disk);
  // A: No other values need to be updated

  return 0;
}

//A: Remove directory from disk, if it is not empty remove all files
//	 Returns 0 on success, -1 on error
int SimpleFS_removeDir(DirectoryHandle* d, Inode* ffb, DiskDriver* disk, ArrayEntry dcbs_to_be_freed[]){
   if(!d || !ffb || !disk)  return -1;

  int ret; //, index = SimpleFS_existFile(d, ffb->fcb.name);
  // A: the file exists, has been checked before

  // I: Start scanning the Inode of the directory d
  int entries = ffb->num_entries, it = 0;
  //printf("entries = %d\n", entries);

	//A: If it's not an empty dir... if(entries > 0)
    for(int i=0; it<entries; i++) {
        DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, ffb, i);

        if(!dir_table_entry_is_valid(dte))  continue;
        it++;
        SimpleFS_remove(d, dte.filename, dcbs_to_be_freed); //A: remove all files recursively
    }

      //A: it's an empty dir... I continue here
      // I: Gets parent
      Inode ffb_parent = {{0}};

      // I: Checks if I'm deleting current dir in DirectoryHandle
      if(ffb == d->dcb)
        ffb_parent = *(d->directory);

      else {
          if (DiskDriver_readBlock(disk, &ffb_parent, ffb->fcb.directory_block) < 0) {
            ERROR_HELPER(-1,"Unable to read parent dir block\n");
          }
      }

      //printf("ffb->fcb.name = %s\n", ffb->fcb.name);    // I: Debug
      int pos_in_parent = ffb->fcb.pos_in_parent;

      // I: Removes list of internal free blocks (they are stored on disk as IndexesBlocks)
      ret = remove_internal_free_blocks(disk, ffb);
      if(ret < 0) {
        return -1;
      }

      //A: Free block of the dir to delete in disk
      ret = DiskDriver_freeBlock(disk, ffb->fcb.block_in_disk);
      ERROR_HELPER(ret,"Problem in remove dir\n");
      DiskDriver_flush(disk);

      //A: Update parent
      ret = remove_directory_entry(disk, &ffb_parent, pos_in_parent);
      if(ret < 0) {
        return -1;
      }

      if(is_an_internal_block(disk, &ffb_parent, pos_in_parent)) {
        // I: pos_in_parent+1 to distinguish element in the array indexes from the init with zeros.
        // Remember: first block in a dir has pos in parent equals to zero
        ret = add_entry_in_internal_free_blocks(disk, &ffb_parent, pos_in_parent+1);
        if(ret < 0) {
            return -1;
        }
      }

      // A: Update parent on disk
      ret = DiskDriver_writeBlock(disk, &ffb_parent, ffb_parent.fcb.block_in_disk);
      ERROR_HELPER(ret,"Problem in remove dir\n");
      DiskDriver_flush(disk);
      //A: No other values need to be updated

  return 0;
}

// I: Prints the pwd of the current dir "d"
// Concatenates parent_dir_name/current_dir_name recursively with side effect on "d"
// Returns -1 on error, 0 otherwise
int SimpleFS_pwd(DirectoryHandle* d, ArrayEntry dcbs_to_be_freed[]) {
    if(!d || !dcbs_to_be_freed)  return -1;

    else if(!d->directory) {
        printf("~");

        // I: Frees current block of dir handle previously allocated (NEW)
        DirectoryBlock* db = d->current_block;
        //printf("db to free = %u\n", db); // I: Debug
        if(db)
            free(db);

        return 0;
    }

    else {
        Inode* dcb = d->dcb;
        if(!dcb)  return -1;

        SimpleFS_changeDir(d, "..", dcbs_to_be_freed); // I: Go to parent
        SimpleFS_pwd(d, dcbs_to_be_freed);
        printf("/%s", dcb->fcb.name);
        return 0;
    }
}

// I: Prints the tree of fs of current dir "d"
// Visits all entries of "d" recursively with side effect on "d"
// Returns -1 on error, 0 otherwise
int SimpleFS_tree(DirectoryHandle* d, ArrayEntry dcbs_to_be_freed[], int depth, int is_last_start_dir_entry) {
    // I: Example
    /*   root
          ├── Dir1
          │    ├── "File3.doc"
          │    └── "File4.docx"
  	      ├── Dir2
          │    └── Dir3
  	      ├── "File1.txt"
  	      └── "File2.odt"*/

    if(!d || !dcbs_to_be_freed || depth<0)      return -1;

    SimpleFS* fs = d->sfs;
    if(!fs)     return -1;

    DiskDriver* disk = fs->disk;
    if(!disk)     return -1;

    Inode* dcb = d->dcb;
    if(!dcb)    return -1;

    // I: Start dir is on top
    if(!depth)    printf("%s\n", dcb->fcb.name);

    // I: Start scanning the Inode of the directory d
    int entries = dcb->num_entries;
    //printf("Entries = %d\n", entries); // I: Debug
    int it = 0;

    for(int i=0; it<entries; i++) {
        //printf("i = %d\n", i); // I: Debug
        //printf("it = %d\n", it); // I: Debug

        DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, dcb, i);

        // I: Checks if there is an internal free block
        if(!dir_table_entry_is_valid(dte))  continue;

        //printf("last_dir_entry(dcb, entries) = %d\n", last_dir_entry(dcb, entries)); // I: Debug
        // I: Flag to know if "i" entry is the last of current dir
        int last_entry = it == entries-1;
        it++;

        // I: Flag to know if "i" entry is also last of start dir
        if(!depth && last_entry)
            is_last_start_dir_entry = 1;

        // I: 2 different prints for last start dir entry & not
        for (int j=0; j < depth; j++)
            is_last_start_dir_entry ? printf("    ") : printf("│   ");


        //printf("i = %d\n", i); // I: Debug
        if(!last_entry)    printf("├── %s\n", dte.filename);
        else               printf("└── %s\n", dte.filename);

        if(dte.is_dir == IS_DIR) {
            //printf("trovata dir, ricorsione!\n");
            SimpleFS_changeDir(d, dte.filename, dcbs_to_be_freed);  // I: Goes to dir
            SimpleFS_tree(d, dcbs_to_be_freed, depth+1, is_last_start_dir_entry);
            SimpleFS_changeDir(d, "..", dcbs_to_be_freed);           // I: Returns to parent (here)
        }
    }

    return 0;
}

// I: Returns last dir entry of directory "dir", -1 on error
int get_last_dir_entry(DiskDriver* disk, Inode* dir, int entries) {
    if(!disk || !dir || entries<0)   return -1;

    int ifb = dir->internal_free_blocks;
    //printf("ifb = %d\n", ifb);    // I: Debug
    // I: Checks if there is an internal free block
    if(!ifb)    return entries-1;
    else {
        // I: Starts searching last entry from the end of the entries
        for(int i=0; i<entries; i++) {
            int last_entry = entries-1-i;

            // A: Retrieves IndexesBlock from disk
            IndexesBlock ib = {0};
            printf("ifb = %d\n", ifb); // I: Debug
            if(DiskDriver_readBlock(disk, &ib, ifb) < 0) {
                printf("Error in get_last_dir_entry: Unable to read IndexesBlock of internal free blocks\n");
                 return -1;
            }

            // A: Searches if the block is in this Indexes block of internal free blocks
            for(int i=0; i<NUM_INDEXES_POINTERS; i++) {
                int block = ib.indexes[i];
                if(block == last_entry)
                    return last_entry;
            }

            // A: Accesses the next Indexes block of internal free blocks
            ifb = ib.next_block;
        }
    }

    return -1;
}

void SimpleFS_ls(DirectoryTableEntry* entries, int size) {
    if(!entries)  return;

    quicksort(entries, 0, size-2);

    printf("Printing all filenames & dirnames in dir\n");
    int i;
    for(i=0; i<size; i++) {
        DirectoryTableEntry dte = entries[i];
        // I: I've init array names with all null dte, so I check if all names have been printed
        if(!dte_is_null(dte)) {
            // I: Debug
            //printf("STOP! i = %d\n",i);
            if(!i)  printf("Dir is empty\n");
            return;
        }

        else if(dte.is_dir == IS_DIR)
            //A: Now the folders are printed in blue
            printf(ANSI_COLOR_BLUE "%s\n" ANSI_COLOR_RESET, dte.filename);
        else
            printf("%s\n", dte.filename);
    }
}

int dte_is_null(DirectoryTableEntry dte) {
    return dte.is_dir == 0 && dte.inode_block == 0 ? 0 : 1;
}

// I: Function to swap two DirectoryTableEntry pointers
void swap(DirectoryTableEntry* a, DirectoryTableEntry* b) {
    DirectoryTableEntry temp = *a;
    *a = *b;
    *b = temp;
}

// I: Quicksort algorithm to order alphabetically an array of DirectoryTableEntry*
void quicksort(DirectoryTableEntry* arr, int l, int r) {
    // I: Base case: No need to sort arrays of length <= 1
    if (l >= r)     return;

    // I: Choose pivot to be the last element in the subarray
    char* pivot = (arr[r]).filename;

    // I: Index indicating the "split" between elements smaller than pivot and
    // elements greater than pivot
    int cnt = l;

    // I: Traverse through array from l to r
    for (int i=l; i<=r; i++) {
        // I: If an element less than or equal to the pivot is found...
        if (strncmp((arr[i]).filename, pivot, MAX_FILENAME_SIZE) <= 0) {
            // I: Then swap arr[cnt] and arr[i] so that the smaller element arr[i]
            // is to the left of all elements greater than pivot
            swap(&(arr[cnt]), &(arr[i]));

            // I: Make sure to increment cnt so we can keep track of what to swap
            // arr[i] with
            cnt++;
        }
    }

    // I: cnt is currently at one plus the pivot's index
    // (Hence, the cnt-2 when recursively sorting the left side of pivot)
    quicksort(arr, l, cnt-2); // I: Recursively sort the left side of pivot
    quicksort(arr, cnt, r);   // I: Recursively sort the right side of pivot
}

// I: Rename file or dir "src" in current dir to "dest"
// Returns 0 on success, -1 on error
int SimpleFS_rename(DirectoryHandle* d, ArrayEntry dcbs_to_be_freed[], char* src, char* dest) {
    if(!d || !src || !dest || strlen(src) >= MAX_FILENAME_SIZE || strlen(dest) >= MAX_FILENAME_SIZE)
        return -1;

    // I: Checks if src exists in current dir and gets its corresponding block
    int block = SimpleFS_existFile(d, src);
    if(block < 0)     return -1;

    SimpleFS* fs = d->sfs;
    if(!fs)     return -1;

    DiskDriver* disk = fs->disk;
    if(!disk)     return -1;

    Inode* dcb = d->dcb;
    if(!dcb)    return -1;

    // I: Checks if I'm renaming current dir
    if(dcb->fcb.block_in_disk == block) {
        strncpy(dcb->fcb.name, dest, MAX_FILENAME_SIZE);
        // printf("dcb->fcb.name = %s\n", dcb->fcb.name);   // I: Debug

        // I: Write block on disk in the block previous got
        if(DiskDriver_writeBlock(disk, dcb, block) < 0) {
            return -1;
        }
        DiskDriver_flush(disk);

        Inode* parent = d->directory;
        if(parent) {
            // Updates DirectoryTableEntry in parent inode
            int pos = dcb->fcb.pos_in_parent;
            DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, parent, pos);
            strncpy(dte.filename, dest, MAX_FILENAME_SIZE);
            add_entry_in_DirectoryTable_by_Inode(disk, parent, pos, 0, &dte);

            // I: Update parent dir inode block writing on disk
            if(DiskDriver_writeBlock(disk, parent, parent->fcb.block_in_disk) < 0) {
              printf("Error in rename: cannot update the directory block\n");
               // A: Remove block written before
				if(DiskDriver_freeBlock(disk,block) < 0)
		  			printf("Error in free block \n");
				DiskDriver_flush(disk);
              return -1;
            }
            DiskDriver_flush(disk);
        }

    }

    // I: Else I'm renaming a file or dir in current dir
    else {
        //Inode* fdb = malloc(sizeof(Inode));
        Inode ffb = {{0}};
        if(DiskDriver_readBlock(disk, &ffb, block) < 0)     return -1;

        strncpy(ffb.fcb.name, dest, MAX_FILENAME_SIZE);

        // I: Write block on disk in the block previous got
        if(DiskDriver_writeBlock(disk, &ffb, block) < 0) {
          return -1;
        }
        DiskDriver_flush(disk);


        // Updates DirectoryTableEntry in current dir inode
        int pos = ffb.fcb.pos_in_parent;
        DirectoryTableEntry dte = get_entry_in_DirectoryTable(disk, dcb, pos);
        strncpy(dte.filename, dest, MAX_FILENAME_SIZE);
        add_entry_in_DirectoryTable_by_Inode(disk, dcb, pos, 0, &dte);

        // I: Questo per salvare le modifiche per i successivi avvii, per ora non funziona correttamente
        //printf("in mkdir: fdb->fcb.block_in_disk = %d\n", fdb->fcb.block_in_disk); // I:Debug
        // I: Update dir inode block writing on disk
        if(DiskDriver_writeBlock(disk, dcb, dcb->fcb.block_in_disk) < 0) {
          printf("Error in rename: cannot update the directory block\n");
          // A: Remove free block inserted before
			if(DiskDriver_freeBlock(disk,block) < 0)
		  		printf("Error in free block \n");
			DiskDriver_flush(disk);
          return -1;
        }
        DiskDriver_flush(disk);
    }

    edit_name(dcbs_to_be_freed, src, dest);
    return 0;
}



// --------------- LIST APIs ---------------

// I: Returns the head info of the list
int get_head_info(List* l) {
    if(!l)  return -1;

    ListEntry* h = l->head;
    if(!h)  return -1;

    return h->free_block;
}

// I: Removes the head from the list and frees the allocated mem
int remove_head(List* l) {
    if(!l)  return -1;

    ListEntry* h = l->head;
    if(!h)  return -1;

    l->head = (ListEntry*) h->next;
    l->size --;
    free(h);
    return 0;
}

// I: Add to list avoiding insert duplicates
int add_to_list(List* l, int free_block) {
    if(!l || free_block<0)  return -1;

    ListEntry* h = (ListEntry*) l->head;
    if(!h) {
        // I: Add as head
        l->head = new_ListEntry(free_block);
        l->size = 1;
        return 0;
    }

    // I: Checks if the new free block is a duplicate (this must be never true)
    if(h->free_block == free_block)    return -1;

    ListEntry* it = h;
    while(it->next) {
        it = (ListEntry*) it->next;
        if(it->free_block == free_block)    return -1;
    }

    // I: Add in tail
    it->next = (struct ListEntry*) new_ListEntry(free_block);
    l->size ++;
    return 0;
}

// I: Creates a new ListEntry, init with "free_block"
ListEntry* new_ListEntry(int free_block) {
    ListEntry* new_le = malloc(sizeof(ListEntry));
    new_le->free_block = free_block;
    new_le->next = NULL;
    return new_le;
}

// I: Checks if list contains the free block, returns 0 if block not in list
int contains(List* l, int free_block) {
    if(!l || free_block<0)  return -1;

    ListEntry* h = l->head;
    if(!h)  return -1;

    ListEntry* it = h;
    while(it->next) {
        it = (ListEntry*) it->next;
        if(it->free_block == free_block)    return 1;
    }

    return 0;
}
