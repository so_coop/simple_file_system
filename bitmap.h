#pragma once
#include <stdint.h>

#define BLOCK_SIZE_BITMAP 8

typedef struct{
  int num_bits;
  char* entries;
}  BitMap;

typedef struct {
  int entry_num;
  char bit_num;
} BitMapEntryKey;

// converts a block index to an index in the array,
// and a char that indicates the offset of the bit inside the array
BitMapEntryKey BitMap_blockToIndex(int num);

// converts a bit to a linear index
int BitMap_indexToBlock(int entry, uint8_t bit_num);

// returns the index of the first bit having status "status"
// in the bitmap bmap, and starts looking from position start
int BitMap_get(BitMap* bmap, int start, int status);

// sets the bit at index pos in bmap to status
int BitMap_set(BitMap* bmap, int pos, int status);

//------------FUNZIONI AGGIUNTIVE-----------

// Creates a new static bitmap
BitMap new_bitmap(int bitmap_blocks,char* bitmap_data);

// Checks if a bitmap is valid, it returns 1 on success & 0 otherwise
int bitmap_is_valid(BitMap b);

// Puts in bits & entries the new position & offset
void code(int* bits, char* entries, int n);

// Returns the block index from bitmap position & offset
int decode(int n, uint8_t bit_num);

// Returns the status in the byte from bit
int BitMap_getBit(BitMap* bmap,int index);

// Print bitmap entry
void printbinchar(char number,int num_bits);

// Print bitmap
void bitmap_print(BitMap* bitmap);

