# **SIMPLEFS**
Project for Sistemi Operativi course, 2018-2019 ed., at *La Sapienza University of Rome*.

*SimpleFS* is a Simple File System interface using binary files, based on ext4 file system for Linux and its statement *“everything is a file”*.

- The file system reserves the first part of the file
     to store:
     - a header section about disk
     - a bitmap to get info about blocks status on disk
     - a single global directory (root)

- Inodes:
    - Every file or directory is associated with an inode that stores its info and its pointers to data or directory blocks

- Blocks are of two types:
    - data blocks
    - directory blocks

A data block is "random" information,
a directory block contains a table in which are stored the inode number block of the file/directory, its name and if the inode refers to a file or a directory.

**How to try it?**

We provide a Unix like a shell to interact with our simple file system, without any installation.

Once cloning the repo, clicking on *shell.sh* will open a terminal window; typing help, you will know which commands are supported.

Read our [wiki](https://gitlab.com/so_coop/simple_file_system/wikis/Home) to know more about fs structure and usage.

© All rights reserved, 666TheNumberOfTheBeast & Andreasta
     
