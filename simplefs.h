#pragma once
#include "bitmap.h"
#include "disk_driver.h"

#define MAX_FILENAME_SIZE 128
// I: An inode stores the max possible direct pointers to fill a block of BLOCK_SIZE

// I: An inode stores the max possible direct pointers to fill a block of BLOCK_SIZE
#define NUM_DIRECT_POINTERS ((BLOCK_SIZE /*- sizeof(long)*/ - sizeof(FileControlBlock) - 2*sizeof(int)) / sizeof(int))

// I: An indirect indexes block stores the max possible indirect pointers to fill a block of BLOCK_SIZE
// this number is greater than NUM_DIRECT_POINTERS and multiple IndirectIndexesBlock can be used while
// IndexesBlock can be used once for each inode
#define NUM_INDEXES_POINTERS ((BLOCK_SIZE - 4*sizeof(int)) / sizeof(int))


#define MAX_FILE_BLOCKS_IN_DIR_BLOCK  ((BLOCK_SIZE - 2*sizeof(int)) / sizeof(DirectoryTableEntry))

#define BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK       NUM_INDEXES_POINTERS*MAX_FILE_BLOCKS_IN_DIR_BLOCK
#define BLOCKS_ADDRESSABLE_BY_DIR_BLOCK_DIRECT_POINTERS            NUM_DIRECT_POINTERS*BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DIR_BLOCK
#define BLOCKS_ADDRESSABLE_BY_A_DIR_BLOCK_INDIRECT_POINTER         NUM_INDEXES_POINTERS*MAX_FILE_BLOCKS_IN_DIR_BLOCK


// I: -1 if FirstBlock is not used to store FileControlBlock
#define MAX_DATA_IN_DATA_BLOCK        (BLOCK_SIZE - 2*sizeof(int))

#define SPACE_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK       NUM_INDEXES_POINTERS*MAX_DATA_IN_DATA_BLOCK
#define SPACE_ADDRESSABLE_BY_DATA_BLOCK_DIRECT_POINTERS            NUM_DIRECT_POINTERS*MAX_DATA_IN_DATA_BLOCK
#define SPACE_ADDRESSABLE_BY_A_DATA_BLOCK_INDIRECT_POINTER         NUM_INDEXES_POINTERS*MAX_DATA_IN_DATA_BLOCK

#define BLOCKS_ADDRESSABLE_BY_A_INDEXES_BLOCK_OF_A_DATA_BLOCK      NUM_INDEXES_POINTERS
#define BLOCKS_ADDRESSABLE_BY_DATA_BLOCK_DIRECT_POINTERS           NUM_DIRECT_POINTERS
#define BLOCKS_ADDRESSABLE_BY_A_DATA_BLOCK_INDIRECT_POINTER        NUM_INDEXES_POINTERS


/*================================================ STRUCTURE ================================================

╔═════════════════════════╗
║          INODE          ║
╠═════════════════════════╣
║ Direct indexes blocks[] ║
╠═════════════════════════╣
║    File control block   ║
╠═════════════════════════╣
║       Num entries       ║
╠═════════════════════════╣
║   Internal free blocks  ║
╚═════════════════════════╝

╔════════════════╗
║  INDEXES BLOCK ║
╠════════════════╣
║    Indexes[]   ║
╠════════════════╣
║ Previous block ║
╠════════════════╣
║   Next block   ║
╠════════════════╣
║  Block in file ║
╠════════════════╣
║  Block in disk ║
╚════════════════╝


IT'S A TREE
SIBLINGS IN THE ARRAY
CHILDREN IN THE POINTERS CONTENT

                                       ...
                                        ^
                                        |
                         Previous block | Next block
                                        |
                                        v
                                +-------+-------+   NUM_INDEXES_POINTERS   +------------+
                                | Indexes Block +------------------------->+ Data Block |
                                +-------+-------+                          +------------+
                                        ^
                                        |
                         Previous block | Next block
                                        |
                                        v
+-------+  NUM_DIRECT_POINTERS  +-------+-------+   NUM_INDEXES_POINTERS   +------------+
| Inode +---------------------->+ Indexes Block +------------------------->+ Data Block |
+-------+                       +-------+-------+                          +------------+
                                        ^
                                        |
                         Previous block | Next block
                                        |
                                        v
                                       ...


The current implementation allows files/dir to have a variable size/number of entries,
each one can grow unlimitedly until disk blocks are available.

An Inode of a file has pointers to DataBlocks which stores data arrays
An Inode of a directory has pointers to DirectoryBlocks which stores tables of pointers to directory entries.
In these tables there is an association among filename, its inode block and its inode type.
In this way, we have a faster lookup of directory entries
To know more info about an entry we have to access the corresponding inode


╔═══════════════════╗
║  DIRECTORY BLOCK  ║
╠═══════════════════╣
║ Directory table[] ║
╠═══════════════════╣
║   Block in file   ║
╠═══════════════════╣
║   Block in disk   ║
╚═══════════════════╝


╔═════════════════════════════════╗
║       DIRECTORY TABLE ENTRY     ║
╠══════════╦═════════════╦════════╣
║ Filename ║ Inode block ║ Is_dir ║
╠══════════╬═════════════╬════════╣
║   dir1   ║      5      ║    1   ║
╠══════════╬═════════════╬════════╣
║   dir2   ║      23     ║    1   ║
╠══════════╬═════════════╬════════╣
║   file1  ║      12     ║    0   ║
╚══════════╩═════════════╩════════╝



Credits: I
===========================================================================================================*/


//------------STRUTTURE AGGIUNTIVE-----------

// OLD
// I: Struct useful to store blocks that have been freed by remove API.
// When a file or dir has been removed, in dir table of inode in which the file or dir was contained,
// a "hole" appears. Each hole represents an internal fragmentation of the disk.
// To avoid this, we mem a new free block in the list if it's not the last entry of the dir table
typedef struct {
  int free_block;
  struct ListEntry* next;
} ListEntry;

typedef struct {
  ListEntry* head;
  int size;
} List;

//------------FINE-----------
// OLD


/*these are structures stored on disk*/

// header, occupies the first portion of each block in the disk
// represents a chained list of blocks
/*typedef struct {
  int previous_block; // chained list (previous block)
  int next_block;     // chained list (next_block)
  int block_in_file;  // position in the file, if 0 we have a file control block
} BlockHeader;*/

// I: Each entry of a directory is stored as a pair filename-inode_block
typedef struct {
  char filename[MAX_FILENAME_SIZE];
  int  inode_block;
  int  is_dir; // I: Versione 2.0
} DirectoryTableEntry;

// I: Stores data file
typedef struct {
  char data[BLOCK_SIZE - 2*sizeof(int)];
  int  block_in_file;          // I: position in the file, if 0 we have a file control block
  int  block_in_disk;          // I: position in the disk
} DataBlock;

#define DIRECTORY_TABLE_ENTRIES_IN_A_DIR_BLOCK MAX_FILE_BLOCKS_IN_DIR_BLOCK //(BLOCK_SIZE - 2*sizeof(int)) / sizeof(DirectoryTableEntry)

// I: This a block of a directory
typedef struct {
  DirectoryTableEntry file_blocks[DIRECTORY_TABLE_ENTRIES_IN_A_DIR_BLOCK];
  int block_in_file;          // I: position in the file, if 0 we have a file control block
  int block_in_disk;          // I: position in the disk
  char padding[BLOCK_SIZE - (DIRECTORY_TABLE_ENTRIES_IN_A_DIR_BLOCK*sizeof(DirectoryTableEntry)) - 2*sizeof(int)]; // I: SOL TEMP
} DirectoryBlock;

// this is in the first block of a chain, after the header
typedef struct {
  int directory_block; // first block of the parent directory
  int block_in_disk;   // repeated position of the block on the disk
  int pos_in_parent;   // I: Position of the block in the parent directory inode
  char name[MAX_FILENAME_SIZE];
  int size_in_bytes;
  int size_in_blocks;
  int is_dir;          // 0 for file, 1 for dir
} FileControlBlock;

// this is the first physical block of a file
// it has a header
// an FCB storing file infos
// and can contain some data


// I: Struct to store a block of indexes of the inode,
// it contains NUM_INDEXES_POINTERS to data or directory blocks.
// Pointers are obtained by storing block position in disk of the Datablocks or DirectoryBlocks,
// so they are called indexes
typedef struct {
  int indexes[NUM_INDEXES_POINTERS];    // I: This stores pointers to data or directory block using their block position in disk
  int previous_block;                   // chained list (previous block)
  int next_block;                       // chained list (next_block)
  int block_in_file;                    // position in the file, if 0 we have a file control block
  int block_in_disk;                    // I: position in the disk
} IndexesBlock;

// I: New version of inode
// It contains NUM_DIRECT_POINTERS to block of indexes,
// each one contains NUM_INDEXES_POINTERS to data or directory blocks.
// Pointers are obtained by storing block position in disk of the IndexesBlock.
// If it is not enough, can be used several IndexesBlocks adding them as next of the last indexes block
// in the array (indirect indexes).
// The array allows fast disk ops, without scanning the first NUM_DIRECT_POINTERS elements of the chained list
typedef struct {
    //long id;
    int direct_indexes_blocks[NUM_DIRECT_POINTERS];  // I: This stores pointers to block of indexes
    FileControlBlock fcb;
    int num_entries;                                 // I: Only for directories
    int internal_free_blocks;                        // I: Only for directories, used to store internal blocks freed by remove API

// When a file or dir has been removed, in dir table of inode in which the file or dir was contained,
// a "hole" appears. Each hole represents an internal fragmentation of the disk.
// To avoid this, we mem a new free block (position in parent) in the list,
// if it's not the last entry of the dir table.
} Inode;


typedef struct {
  DiskDriver* disk;
  // add more fields if needed
} SimpleFS;

// this is a file handle, used to refer to open files
typedef struct {
  SimpleFS* sfs;                   // pointer to memory file system structure
  Inode* fcb;                      // pointer to the inode of the file(read it)
  Inode* directory;                // pointer to the inode directory where the file is stored
  DataBlock* current_block;        // current block in the file
  int pos_in_file;                 // position of the cursor
} FileHandle;

typedef struct {
  SimpleFS* sfs;                   // pointer to memory file system structure
  Inode* dcb;                      // pointer to the inode of the directory(read it)
  Inode* directory;                // pointer to the parent directory (null if top level)
  DirectoryBlock* current_block;   // current block in the directory
  int pos_in_dir;                  // absolute position of the cursor in the directory
  int pos_in_block;                // relative position of the cursor in the block
} DirectoryHandle;



//------------STRUTTURE AGGIUNTIVE-----------

// I: Struct useful to contain directory inodes (dcbs) when changeDir is called, to avoid mem leaks.
// It is used in test or shell to free mem areas allocated in changeDir calls
typedef struct {
  Inode* dcb;
  int    to_be_closed;      // I: 0 equals to no, 1 equals to yes
} ArrayEntry;

//------------FINE-----------



// initializes a file system on an already made disk
// returns a handle to the top level directory stored in the first block
DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk, ArrayEntry dcbs_to_be_freed[]);

// creates the inital structures, the top level directory
// has name "/" and its control block is in the first position
// it also clears the bitmap of occupied blocks on the disk
// the current_directory_block is cached in the SimpleFS struct
// and set to the top level directory
void SimpleFS_format(SimpleFS* fs);

// creates an empty file in the directory d
// returns null on error (file existing, no free blocks)
// an empty file consists only of a block of type FirstBlock
FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename);

// reads in the (preallocated) blocks array, the name of all files in a directory
// I: Returns 0 on success, -1 on error
int SimpleFS_readDir(DirectoryTableEntry* names, DirectoryHandle* d);


// opens a file in the  directory d. The file should be exisiting
FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename);


// closes a file handle (destroyes it)
int SimpleFS_close(FileHandle* f);

// writes in the file, at current position for size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes written
int SimpleFS_write(FileHandle* f, void* data, int size);

// writes in the file, at current position size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes read
int SimpleFS_read(FileHandle* f, void* data, int size);

// returns the number of bytes read (moving the current pointer to pos)
// returns pos on success
// -1 on error (file too short)
int SimpleFS_seek(FileHandle* f, int pos);

// seeks for a directory in d. If dirname is equal to ".." it goes one level up
// 0 on success, negative value on error
// it does side effect on the provided handle
int SimpleFS_changeDir(DirectoryHandle* d, char* dirname, ArrayEntry dcbs_to_be_freed[]);

// creates a new directory in the current one (stored in fs->current_directory_block)
// 0 on success
// -1 on error
int SimpleFS_mkDir(DirectoryHandle* d, char* dirname);

// removes the file in the current directory
// returns -1 on failure 0 on success
// if a directory, it removes recursively all contained files
//int SimpleFS_remove(SimpleFS* fs, char* filename);
int SimpleFS_remove(DirectoryHandle* d, char* filename, ArrayEntry dcbs_to_be_freed[]);


//------------FUNZIONI AGGIUNTIVE-----------

int SimpleFS_existFile(DirectoryHandle* d, const char* filename);

// GET
DataBlock* get_entry_in_Inode_about_file(DiskDriver* disk, Inode* fdb, int block);
DataBlock get_entry_in_Inode_about_file_statically(DiskDriver* disk, Inode* fdb, int block);
int get_indexes_block_pointer(DiskDriver* disk, Inode* fdb, int pos);
IndexesBlock get_indexes_block(DiskDriver* disk, Inode* fdb, int pos);
int dir_table_entry_is_valid(DirectoryTableEntry dte);
DirectoryTableEntry get_entry_in_DirectoryTable(DiskDriver* disk, Inode* fdb, int block);
int get_inode_block_in_DirectoryBlock(DiskDriver* disk, Inode* fdb, int block);

// ADD DIR
int add_entry_in_DirectoryTable_by_Inode(DiskDriver* disk, Inode* fdb, int block, int block_in_disk, DirectoryTableEntry* dte);
int indexes_block_is_valid(IndexesBlock ib);
IndexesBlock new_indexes_block(DiskDriver* disk, Inode* fdb, int block_in_disk, int block_in_file);
DirectoryBlock new_dir_block(DiskDriver* disk, Inode* fdb, IndexesBlock ib, int block_in_disk, int block_in_file);
int dir_block_is_valid(DirectoryBlock db);
int add_entry_in_DirectoryTable_by_DirectoryBlock(DiskDriver* disk, DirectoryBlock* db, int block, DirectoryTableEntry* dte);
int add_directory_entry(DiskDriver* disk, Inode* fdb, int block_in_dir, int block_in_disk, const char* filename, int type);

// REMOVE
int remove_directory_entry(DiskDriver* disk, Inode* fdb, int block);
int remove_entry_in_DirectoryTable_by_Inode(DiskDriver* disk, Inode* fdb, int block);

// ADD FILE
int add_DataBlock_entry_in_Inode(DiskDriver* disk, Inode* ffb, int block, int block_in_disk, DataBlock* ndb);
DataBlock* new_data_block(DiskDriver* disk, int block_in_disk, int block_in_file);
int data_block_is_valid(DataBlock db);
DataBlock* add_file_entry(DiskDriver* disk, Inode* ffb,  int block_in_file, int block_in_disk);

int get_free_block(DiskDriver* disk, Inode* dir);
int is_an_internal_block(DiskDriver* disk, Inode* dir, int block);
int SimpleFS_removeFile(DirectoryHandle* d, Inode* ffb, DiskDriver* disk);
int SimpleFS_removeDir(DirectoryHandle* d, Inode* ffb, DiskDriver* disk, ArrayEntry dcbs_to_be_freed[]);

int SimpleFS_pwd(DirectoryHandle* d, ArrayEntry dcbs_to_be_freed[]);
int SimpleFS_tree(DirectoryHandle* d, ArrayEntry dcbs_to_be_freed[], int depth, int is_last_start_dir_entry);
int get_last_dir_entry(DiskDriver* disk, Inode* dir, int entries);
void SimpleFS_ls(DirectoryTableEntry* entries, int size);
int dte_is_null(DirectoryTableEntry dte);
void swap(DirectoryTableEntry* a, DirectoryTableEntry* b);
void quicksort(DirectoryTableEntry* arr, int l, int r);
int SimpleFS_rename(DirectoryHandle* d, ArrayEntry dcbs_to_be_freed[], char* src, char* dest);
void overwrite(Inode* ffb, int block_in_file, int free_space, int size);



DataBlock* get_first_element_in_inode_about_file(DiskDriver* disk, Inode* ffb);
DirectoryBlock* get_first_element_in_inode_about_dir(DiskDriver* disk, Inode* ffb);



int add(ArrayEntry a[], Inode* e);
int edit_name(ArrayEntry a[], char* src, char* dest);
int get_head_info(List* l);
int remove_head(List* l);
int add_to_list(List* l, int free_block);
ListEntry* new_ListEntry(int free_block);
int contains(List* l, int free_block);

