#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bitmap.h"
#include "tools.h"
#include "test.h"

int main(int argc, char* argv[]) {

  printf("===================================\n");
  printf("Starting test bitmap_blockToIndex\n");
  printf("===================================\n");

  BitMapEntryKey b1 = BitMap_blockToIndex(N1);
  BitMapEntryKey b2 = BitMap_blockToIndex(N2);
  BitMapEntryKey b3 = BitMap_blockToIndex(N3);
  BitMapEntryKey b4 = BitMap_blockToIndex(N4);
  BitMapEntryKey b5 = BitMap_blockToIndex(N5);
  BitMapEntryKey b6 = BitMap_blockToIndex(N6);

  printf("Block index: %d => Array index: %d offset: %d\n", N1, b1.entry_num, b1.bit_num);
  printf("Block index: %d => Array index: %d offset: %d\n", N2, b2.entry_num, b2.bit_num);
  printf("Block index: %d => Array index: %d offset: %d\n", N3, b3.entry_num, b3.bit_num);
  printf("Block index: %d => Array index: %d offset: %d\n", N4, b4.entry_num, b4.bit_num);
  printf("Block index: %d => Array index: %d offset: %d\n", N5, b5.entry_num, b5.bit_num);
  printf("Block index: %d => Array index: %d offset: %d\n", N6, b6.entry_num, b6.bit_num);

  printf("===================================\n");
  printf("Test bitmap_indexToBlock\n");
  printf("===================================\n");

  int partial = 0;
  int index1 = BitMap_indexToBlock(b1.entry_num, b1.bit_num);
  int index2 = BitMap_indexToBlock(b2.entry_num, b2.bit_num);
  int index3 = BitMap_indexToBlock(b3.entry_num, b3.bit_num);
  int index4 = BitMap_indexToBlock(b4.entry_num, b4.bit_num);
  int index5 = BitMap_indexToBlock(b5.entry_num, b5.bit_num);
  int index6 = BitMap_indexToBlock(b6.entry_num, b6.bit_num);

  printf("Array index: %d offset: %d => Block index: %d\n", b1.entry_num, b1.bit_num, index1);
  if(index1==N1) partial++;
  printf("Array index: %d offset: %d => Block index: %d\n", b2.entry_num, b2.bit_num, index2);
  if(index2==N2) partial++;
  printf("Array index: %d offset: %d => Block index: %d\n", b3.entry_num, b3.bit_num, index3);
  if(index3==N3) partial++;
  printf("Array index: %d offset: %d => Block index: %d\n", b4.entry_num, b4.bit_num, index4);
  if(index4==N4) partial++;
  printf("Array index: %d offset: %d => Block index: %d\n", b5.entry_num, b5.bit_num, index5);
  if(index5==N5) partial++;
  printf("Array index: %d offset: %d => Block index: %d\n", b6.entry_num, b6.bit_num, index6);
  if(index6==N6) partial++;

  printf("\nScore: %d/6\n", partial);
  int total = partial;

  printf("===================================\n");
  printf("Test bitmap_get\n");
  printf("===================================\n");



  //A: manual test for check get function, there aren't point for this test
  int num_bits = 32;
  char* entries = (char*) malloc(sizeof(char)*num_bits/BLOCK_SIZE_BITMAP);
  BitMap bitmap = new_bitmap(num_bits, entries);
  bitmap.entries[0] = 255;
  bitmap.entries[1] = 0;
  bitmap.entries[2] = 140;
  bitmap.entries[3] = 169;

  bitmap_print(&bitmap);

  bitmap_test_get(&bitmap,0,0);
  printf("Expected: 8\n");
  bitmap_test_get(&bitmap,1,0);
  printf("Expected: 8\n");
  bitmap_test_get(&bitmap,12,1);
  printf("Expected: 18\n");
  bitmap_test_get(&bitmap,25,0);
  printf("Expected: 25\n");
  bitmap_test_get(&bitmap,0,1);
  printf("Expected: 0\n");
  bitmap_test_get(&bitmap, num_bits, 1);
  printf("Expected: -1\n");
  bitmap_test_get(&bitmap, num_bits+1, 1);
  printf("Expected: -1\n");

  free(entries);

  printf("===================================\n");
  printf("Test bitmap_set\n");
  printf("===================================\n");

  partial = 0;
  int res;

  int num_bits1 = 16;
  char* entries1 = (char*) malloc(sizeof(char)*num_bits1/BLOCK_SIZE_BITMAP);
  BitMap bitmap1 = new_bitmap(num_bits1, entries1);
  bitmap1.entries[0] = N7;
  bitmap1.entries[1] = N8;
  printf("\nTest 1:\n");
  bitmap_print(&bitmap1);

  bitmap_test_set(&bitmap1,0,0);
  res = bitmap_test_get(&bitmap1,0,0);
  if(res == 0)    partial++;
  printf("Expected: 0\n");

  bitmap_test_set(&bitmap1,12,1);
  res = bitmap_test_get(&bitmap1,12,1);
  if(res == 12)    partial++;
  printf("Expected: 12\n");

  bitmap_test_set(&bitmap1,8,1);
  res = bitmap_test_get(&bitmap1,8,1);
  if(res == 8)    partial++;
  printf("Expected: 8\n");

  bitmap_test_set(&bitmap1,5,0);
  res = bitmap_test_get(&bitmap1,5,0);
  if(res == 5)    partial++;
  printf("Expected: 5\n");

  printf("\nScore: %d/4\n",partial);
  total+=partial;
  partial=0;

  printf("Final bitmap: \n");
  bitmap_print(&bitmap1);
  free(entries1);

  int num_bits2 = 8;
  char* entries2 = (char*) malloc(sizeof(char)*num_bits2/BLOCK_SIZE_BITMAP);
  BitMap bitmap2 = new_bitmap(num_bits2, entries2);
  bitmap2.entries[0] = N9;
  printf("\nTest 2:\n");
  bitmap_print(&bitmap2);

  bitmap_test_set(&bitmap2,0,0);
  res = bitmap_test_get(&bitmap2,0,0);
  if(res == 0)    partial++;
  printf("Expected: 0\n");

  bitmap_test_set(&bitmap2,7,1);
  res = bitmap_test_get(&bitmap2,7,1);
  if(res == 7)    partial++;
  printf("Expected: 7\n");

  bitmap_test_set(&bitmap2,6,1);
  res = bitmap_test_get(&bitmap2,6,1);
  if(res == 6)    partial++;
  printf("Expected: 6\n");

  bitmap_test_set(&bitmap2,5,0);
  res = bitmap_test_get(&bitmap2,5,0);
  if(res == 5)    partial++;
  printf("Expected: 5\n");

  printf("\nScore: %d/4\n",partial);
  total+=partial;
  partial=0;

  printf("Final bitmap: \n");
  bitmap_print(&bitmap2);
  free(entries2);

  int num_bits3 = 24;
  char* entries3 = (char*) malloc(sizeof(char)*num_bits3/BLOCK_SIZE_BITMAP);
  BitMap bitmap3 = new_bitmap(num_bits3, entries3);
  bitmap3.entries[0] = N10;
  bitmap3.entries[1] = N11;
  bitmap3.entries[2] = N12;

  printf("\nTest 3:\n");
  bitmap_print(&bitmap3);

  bitmap_test_set(&bitmap3,0,0);
  res = bitmap_test_get(&bitmap3,0,0);
  if(res == 0)    partial++;
  printf("Expected: 0\n");

  bitmap_test_set(&bitmap3,9,1);
  res = bitmap_test_get(&bitmap3,9,1);
  if(res == 9)    partial++;
  printf("Expected: 9\n");

  bitmap_test_set(&bitmap3,20,1);
  res = bitmap_test_get(&bitmap3,20,1);
  if(res == 20)    partial++;
  printf("Expected: 20\n");

  bitmap_test_set(&bitmap3,22,0);
  res = bitmap_test_get(&bitmap3,22,0);
  if(res == 22)    partial++;
  printf("Expected: 22\n");

  printf("\nScore: %d/4\n",partial);
  total+=partial;
  partial=0;

  printf("\nFinal bitmap: \n");
  bitmap_print(&bitmap3);
  free(entries3);



  if(total == 18)   printf(ANSI_COLOR_GREEN "\nTEST PASSED\n" ANSI_COLOR_RESET);
  else              printf(ANSI_COLOR_RED "\nTEST FAILED\n" ANSI_COLOR_RESET);

  return 0;
}

/*BitMap* bitmap_test_new(BitMap* bitmap,int num, int num_bits){
  BitMap *bmap = (BitMap*) malloc(sizeof(BitMap));
  bmap->num_bits = num_bits;
  bmap->entries = (char*) malloc(sizeof(char)*num_bits/BLOCK_SIZE_BITMAP);
  printf("Bitmap n° %d, num bits %d",num,num_bits);
  return bmap;
 }*/

int bitmap_test_get(BitMap* bitmap, int start, int status){
  int res= BitMap_get(bitmap,start,status);
  printf("Searching status %d from index %d => %d\n",status,start,res);
  return res;
}

void bitmap_test_set(BitMap* bitmap, int pos, int status){
  BitMap_set(bitmap, pos, status);
  printf("Setting status %d at index %d\n",status,pos);
}


