#ifndef TOOLS_H_
#define TOOLS_H

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// I: Macro to define colors that can be used in prints
#define ANSI_COLOR_RED     "\x1b[1;31m" // I: It is bright red
#define ANSI_COLOR_GREEN   "\x1b[1;32m" // I: It is bright green
#define ANSI_COLOR_BLUE    "\x1b[1;34m" // I: It is bright blue
#define ANSI_COLOR_RESET   "\x1b[0m"

#define WP_ERR -2 // I: Wrong parameters error

#define getbit(x,bit)   ( ((x)>>(bit)) & 0x01 )
#define setbit(x,bit)   (  (x) |=  (1<<(bit)) )
#define clearbit(x,bit) (  (x) &= ~(1<<(bit)) )
// A: I'm using 1<<bit as unsigned char defined in a variable avoid having to worry about the sign bit

//A: Update... it's a problem with %d of printf


#define GENERIC_ERROR_HELPER(cond, errCode, msg) do {               \
        if (cond) {                                                 \
            fprintf(stderr, "%s: %s\n", msg, strerror(errCode));    \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define ERROR_HELPER(ret, msg)          GENERIC_ERROR_HELPER((ret < 0), errno, msg)



#endif //TOOLS_H
