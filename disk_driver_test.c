#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "disk_driver.h"
#include "tools.h"
#include "test.h"

#define POINTS 43


int main(int argc, char* argv[]) {

  printf("===================================\n");
  printf("Starting test diskDriver\n");
  printf("===================================\n");

  DiskDriver disk;
  const char* filename = "disk_driver_test.dat";
  int num_blocks = 512;

  printf("Initing the disk...\n");
  // I: DiskDriver_init() throws & handles errors itself, it is a void func
  DiskDriver_init(&disk, filename, num_blocks);
  DiskDriver_flush_test(&disk);
  printf("File %s created/opened\n", filename);

  DiskDriver_print_status_test(&disk);

  int total = 0, pos = 0, res;
  char in_buffer[BLOCK_SIZE] = TEXT1;
  char out_buffer[BLOCK_SIZE] = TEXT0;

  //DiskDriver_print_buffers_test(in_buffer, out_buffer);

  // I: First reading
  res = DiskDriver_readBlock_test(&disk,(void*) out_buffer, pos);
  printf(" (Expected: free)\n");
  if(res == -1)
    total++;

  res = DiskDriver_getFreeBlock_test(&disk, pos);
  printf(" (Expected: %d)\n\n", pos);
  if(res == pos)
    total++;

  // I: Writes all disk's blocks
  char* texts[] = {TEXT1, TEXT2, TEXT3, TEXT4, TEXT5, TEXT6};
  int texts_size = 6;

  int i;
  for(i=0; i<texts_size-1; i++) {
      //A: first step WRITE
      strncpy(in_buffer, texts[i], BLOCK_SIZE);
      res = DiskDriver_writeBlock_test(&disk,(void*) in_buffer, i);
      if(!res)
        total++;
      DiskDriver_flush_test(&disk);

      //A: second step READ
      res = DiskDriver_readBlock_test(&disk, out_buffer, i);
      if(!res)
        total++;
      DiskDriver_flush_test(&disk);

      //A: third step final check
      DiskDriver_print_buffers_test(in_buffer, out_buffer);
      if(!strcmp(in_buffer,out_buffer))
        total++;
  }

  DiskDriver_print_status_test(&disk);

  // I: Frees first block
  DiskDriver_freeBlock_test(&disk, pos);

  res = DiskDriver_getFreeBlock_test(&disk, pos);
  printf(" (Expected: %d)\n", pos);
  if(res == pos)
    total++;

  res = DiskDriver_readBlock_test(&disk, out_buffer, pos);
  printf(" (Expected: free)\n");
  if(res == -1)
    total++;

  DiskDriver_flush_test(&disk);

  DiskDriver_print_status_test(&disk);

  //A: file opened again

  DiskDriver_init(&disk, filename, num_blocks);
  DiskDriver_flush_test(&disk);
  printf("\nFile %s opened again\n", filename);

  DiskDriver_print_status_test(&disk);

  for(i=3; i<texts_size-1; i++) {
      //A: first step WRITE
      strncpy(in_buffer, texts[i], BLOCK_SIZE-1); //A: text6 is too big
      res = DiskDriver_writeBlock_test(&disk,(void*) in_buffer, i);
      if(!res)
        total++;
      DiskDriver_flush_test(&disk);

      //A: second step READ
      res = DiskDriver_readBlock_test(&disk, out_buffer, i);
      if(!res)
        total++;
      DiskDriver_flush_test(&disk);

      //A: third step final check
      DiskDriver_print_buffers_test(in_buffer, out_buffer);
      if(!strcmp(in_buffer,out_buffer))
        total++;
  }

  DiskDriver_print_status_test(&disk);

  // I: Now file opened again but with more num_blocks
  DiskDriver_init(&disk, filename, num_blocks+20);
  DiskDriver_flush_test(&disk);
  printf("\nFile %s opened again but with more num_blocks\n", filename);

  DiskDriver_print_status_test(&disk);

  for(i=0; i<texts_size-1; i++) {
      //A: first step WRITE
      strncpy(in_buffer, texts[i], BLOCK_SIZE-1); //A: text6 is too big
      res = DiskDriver_writeBlock_test(&disk,(void*) in_buffer, i);
      if(!res)
        total++;
      DiskDriver_flush_test(&disk);

      //A: second step READ
      res = DiskDriver_readBlock_test(&disk, out_buffer, i);
      if(!res)
        total++;
      DiskDriver_flush_test(&disk);

      //A: third step final check
      DiskDriver_print_buffers_test(in_buffer, out_buffer);
      if(!strcmp(in_buffer,out_buffer))
        total++;
  }

  DiskDriver_print_status_test(&disk);

  res = DiskDriver_readBlock_test(&disk, out_buffer, 5);
  printf(" (Expected: free)\n");
  if(res == -1) total++;
  DiskDriver_flush_test(&disk);
  int first_creation = 0;

  res = DiskDriver_readBlock_test(&disk, out_buffer, 20);
  printf(" (Expected: free)\n");
  if(res == -1) {
    total++;
    DiskDriver_flush_test(&disk);

    res = DiskDriver_writeBlock_test(&disk,(void*) in_buffer, 20);
    if(!res)
        total++;
    DiskDriver_flush_test(&disk);
    first_creation = 2;
  }


  // I: Frees first block again to be consistent with successive tests
  DiskDriver_freeBlock_test(&disk, pos);

  res = DiskDriver_getFreeBlock_test(&disk, pos);
  printf(" (Expected: %d)\n", pos);
  if(res == pos)
    total++;

  res = DiskDriver_readBlock_test(&disk, out_buffer, pos);
  printf(" (Expected: free)\n");
  if(res == -1)
    total++;

  DiskDriver_flush_test(&disk);

  DiskDriver_print_status_test(&disk);

  // I: If file is created for the 1st time there are 2 more points
  int points = POINTS + first_creation;
  printf("Score: %d/%d\n\nOpen disk_driver_test.dat to see if disk status is consistent\n", total, points);
  if(total == points)   printf(ANSI_COLOR_GREEN "\nTEST PASSED\n" ANSI_COLOR_RESET);
  else                  printf(ANSI_COLOR_RED "\nTEST FAILED\n" ANSI_COLOR_RESET);

  return 0;
}


void DiskDriver_print_status_test(DiskDriver* disk) {
  printf("\n***** Disk status *****\n");
  DiskDriver_print_status(disk);
  printf("***********************\n\n");
}

void DiskDriver_print_buffers_test(char in_buffer[], char out_buffer[]) {
  printf("\n***** I/O BUFFERS *****\n");
  printf("in_buffer = %s\n", in_buffer);
  printf("out_buffer = %s\n", out_buffer);
  printf("***********************\n\n");
}

int DiskDriver_readBlock_test(DiskDriver* disk, void* dest, int block_num){
  printf("Reading from the disk...\n");
  int res = DiskDriver_readBlock(disk, dest, block_num);
  if(res == WP_ERR)         printf("Error DiskDriver_writeBlock, wrong parameters\n");
  else if(res == -1)        printf("Block at pos %d is free\n", block_num);
  else                      printf("Block at pos %d is occupied\n", block_num);
  return res;
}


int DiskDriver_writeBlock_test(DiskDriver* disk, void* src, int block_num){
  printf("Writing on the disk...\n");
  int res = DiskDriver_writeBlock(disk, src, block_num);
  if(res == WP_ERR)         printf("Error DiskDriver_writeBlock, wrong parameters\n");
  else if(res == -1)        printf("Error DiskDriver_writeBlock, block is occupied\n");
  else                      printf("Written in the disk in position %d\n", block_num);
  return res;
 }



void DiskDriver_freeBlock_test(DiskDriver* disk, int block_num){
  int res = DiskDriver_freeBlock(disk, block_num);
  if(res)             printf("Error DiskDriver_freeBlock, wrong parameters\n");
  else                printf("Freed block num %d\n", block_num);
}


int DiskDriver_getFreeBlock_test(DiskDriver* disk, int start){
  int res = DiskDriver_getFreeBlock(disk, start);
  if(res == -1)       printf("Error DiskDriver_getFreeBlock, wrong parameters\n");
  else                printf("The first free block in the disk from position %d is %d\n", start, res);
  return res;
}

void DiskDriver_flush_test(DiskDriver* disk){
  // printf("\nFlushing data...\n");
  int res = DiskDriver_flush(disk);
  if(res)             printf("Error DiskDriver_flush, wrong parameters\n");
  else                printf("Data flushed\n");
}
